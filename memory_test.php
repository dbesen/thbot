<?
	require_once("code/autoloader.php");

	$m = memory_get_usage();
	for($i=0;$i<10000;$i++) {
		$p1 = new Player("Player 1");
		$p2 = new Player("Player 2");
		$g = new Game();
		$g->addPlayer($p1);
		$g->addPlayer($p2);
		$g->beginGame();
		$g->postBlinds();
		$g->call($p1);
		$g->check($p2);
		// flop
		$g->check($p2);
		$g->check($p1);
		// turn
		$g->check($p2);
		$g->check($p1);
		// river
		$g->check($p2);
		$g->check($p1);
		if($i == 10)
			print (memory_get_usage() - $m) . "\n";
		if($i == 100)
			print (memory_get_usage() - $m) . "\n";
		if($i == 1000)
			print (memory_get_usage() - $m) . "\n";
	}
	print (memory_get_usage() - $m) . "\n";
?>
