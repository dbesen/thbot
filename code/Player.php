<?
	class Player {
		private $name;
		private $cards = array();
		private $money = 0;

		private $folded = false;
		private $allIn = false;
		private $eliminated = false;

		public function __construct($name) {
			$this->name = $name;
		}

		public function getName() {
			return $this->name;
		}

		public function addCard(Card $card) {
			$this->cards[] = $card;
		}

		public function clearCards() {
			$this->cards = array();
		}

		public function getCards() {
			return $this->cards;
		}

		public function addMoney($amt) {
			if($amt + $this->money < 0) {
				throw new Exception("Negative money!");
			}
			$this->money += $amt;
		}

		public function getMoney() {
			return $this->money;
		}

		public function equals(Player $p) {
			if($this->name == $p->name) return true;
			return false;
		}

		public function getCardStr() {
			return $this->cards[0] . " " . $this->cards[1];
		}

		public function folded($f = null) {
			if($f === true) $this->folded = true;
			else if($f === false) $this->folded = false;
			else return $this->folded;
		}

		public function allIn($a = null) {
			if($a === true) $this->allIn = true;
			else if($a === false) $this->allIn = false;
			else return $this->allIn;
		}

		public function active() {
			return !($this->folded() || $this->allIn());
		}

		public function eliminated($e = null) {
			if($e === true) $this->eliminated = true;
			else if($e === false) $this->eliminated = false;
			else return $this->eliminated;
		}

		public function __toString() {
			return $this->name;
		}
	}
?>
