<?
	require_once('Net/SmartIRC.php');

	class THBot {
		private $channel = '#test';
		private $irc;
		private $game_start_timer;
		private $irch;

		function __construct() {
			$this->irch = new IrcHandler(array($this, "pubmsg"), array($this, "privmsg"));
			$this->irc = new Net_SmartIRC();

			// $this->irc->setDebug(SMARTIRC_DEBUG_ALL);
			$this->irc->setUseSockets(TRUE);
			$this->irc->setSenddelay(0);

			$this->irc->registerActionhandler(SMARTIRC_TYPE_CHANNEL, '', $this, 'onChannelMessage');
			$this->irc->registerActionhandler(SMARTIRC_TYPE_QUERY, '', $this, 'onPrivateMessage');

			$this->irc->connect('localhost', 6667);
			$this->irc->login('thbot', 'Texas Hold \'Em', 0, 'thbot');
			$this->irc->join($this->channel);
			$this->irc->listen();
			$this->irc->disconnect();
		}

		function onChannelMessage($irc, $data) {
			// $data->message
//			$irc->message(SMARTIRC_TYPE_CHANNEL, $data->channel, $data->nick.': pong');
			if($data->message == "!holdem") {
				$this->game_start_timer = $this->irc->registerTimehandler(30000, $this, 'start_game');
			}
			$this->irch->publicMessage($data->nick, $data->message);
		}

		function start_game() {
        		$this->irc->unregisterTimeid($this->game_start_timer);
			$this->irch->beginGame();
		}

		function onPrivateMessage($irc, $data) {
//			$irc->message(SMARTIRC_TYPE_CHANNEL, $this->channel, $data->nick.': pong');
			$this->irch->privateMessage($data->nick, $data->message);
		}

		function pubmsg($text) {
			$this->irc->message(SMARTIRC_TYPE_CHANNEL, $this->channel, $text);
		}

		function privmsg($nick, $text) {
			$this->irc->message(SMARTIRC_TYPE_NOTICE, $nick, $text);
		}

	}
?>
