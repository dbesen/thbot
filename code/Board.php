<?
	class Board {
		private $cards = array();

		public function addCard(Card $card) {
			$this->cards[] = $card;
		}

		public function clearCards() {
			$this->cards = array();
		}

		public function getCards() {
			return $this->cards;
		}

		public function getHand($player) {
			$c = $this->cards;
			$c = array_merge($c, $player->getCards());
			return new Hand($c);
		}

		public function __toString() {
			$s = array();
			foreach($this->cards as $card) {
				$s[] = $card->__toString();
			}

			return join(" ", $s);
		}
		
	}
?>
