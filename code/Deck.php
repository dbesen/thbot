<?
	class Deck {
		private $cards;
		private $stack = null;
		public function __construct() {
			$this->shuffle();
		}

		public function dealCard() {
			if(count($this->cards) == 0) return null;
			return new Card(array_pop($this->cards));
		}

		public function stack($s) {
			$this->stack = $s;
			$this->shuffle();
		}

		public function shuffle() {
			$this->cards = array();
			$ranks = "A23456789TJQK";
			$suits = "cdhs";
			$ranks = str_split($ranks);
			$suits = str_split($suits);
			foreach($ranks as $rank) {
				foreach($suits as $suit) {
					$this->cards[] = $rank . $suit;
				}
			}
			shuffle($this->cards);

			// Stack (for testing)
			if($this->stack != null)
				$this->cards = array_merge($this->cards, array_reverse(explode(" ", $this->stack)));
		}
	}
?>
