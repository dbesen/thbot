<?
	class Card {
		private $suit;
		private $rank;

		function __construct($card) {
			if(strlen($card) != 2) throw new Exception("Invalid card $card");
			$card = strtolower($card);
			$this->rank = $card{0};
			$this->suit = $card{1};
			$ranks = "23456789tjqka";
			if(!strstr($ranks, $this->rank)) throw new Exception("Invalid rank");
			$suits = "hdcs";
			if(!strstr($suits, $this->suit)) throw new Exception("Invalid suit");
		}

		function __toString() {
			return strtoupper($this->rank) . $this->suit;
		}

		function compareTo(Card $b) {
			$ranks = "23456789tjqka";
			return strpos($ranks, $this->rank) - strpos($ranks, $b->rank);
		}

		function rankStr() {
			switch($this->rank) {
				case 2: return "Two";
				case 3: return "Three";
				case 4: return "Four";
				case 5: return "Five";
				case 6: return "Six";
				case 7: return "Seven";
				case 8: return "Eight";
				case 9: return "Nine";
				case 't': return "Ten";
				case 'j': return "Jack";
				case 'q': return "Queen";
				case 'k': return "King";
				case 'a': return "Ace";
			}
			throw new Exception("Invalid rank");
		}

		function suitStr() {
			switch($this->suit) {
				case 'h': return 'Hearts';
				case 'c': return 'Clubs';
				case 'd': return 'Diamonds';
				case 's': return 'Spades';
			}
		}

		function getIrcStr() {
			$rank = strtoupper($this->rank);
			switch($this->suit) {
			/* Change foreground color, background gray
				case 'h': return "4,15{$rank}h";
				case 'c': return "3,15{$rank}c";
				case 'd': return "2,15{$rank}d";
				case 's': return "1,15{$rank}s";
			*/
				case 'h': return "01,05{$rank}h";
				case 'c': return "01,03{$rank}c";
				case 'd': return "00,02{$rank}d";
				case 's': return "01,15{$rank}s";
			}
		}
	}
?>
