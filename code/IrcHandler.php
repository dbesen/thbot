<?
	class IrcHandler {
		private $game;
		private $pub_callback;
		private $private_callback;
		private $players;
		private $registration_begun;
		private $game_begun;
		private $round_number;
		private $betting_round;

		private $smallBlind = 5;
		private $bigBlind = 10;
		private $stacksize = 500;

		public function __construct($pub_callback, $private_callback) {
			$this->pub_callback = $pub_callback;
			$this->private_callback = $private_callback;
			$this->reset();
		}

		public function publicMessage($nick, $text) {
			if($text == "!holdem") {
				if(!$this->registration_begun && !$this->game_begun) {
					$this->pubmsg("Starting game in 30 seconds");
					$this->pubmsg('Message me "buy in" to register');
					$this->addPlayer($nick);
					$this->registration_begun = true;
				} else {
					$this->pubmsg("Game already in progress");
				}
			}
			if($text == "!help") {
				$this->pubmsg("!holdem to start a game");
			}
			if($text == "!mem") {
				$this->pubmsg("Using " . number_format(memory_get_usage(true)) . " bytes");
			}
		}

		public function privateMessage($nick, $text) {
			if($this->registration_begun && !$this->game_begun) {
				$text = strtolower($text);
				if($text == "buy in" || $text == "buyin") {
					if($this->findPlayer($nick) != null) {
						$this->privmsg($nick, "You're already registered");
					} else {
						$this->addPlayer($nick);
					}
				}
			}

			if($this->game_begun) {
				$p = $this->findPlayer($nick);
				if($p == null) {
					$this->privmsg($nick, "You're not in the game");
					return;
				}
				if($this->parseCommand($p, $text)) {
					$this->checkAdvance();
				}
			}
		}

		private function checkAdvance() {
			if($this->game->getBettingRound() != $this->betting_round) {
				$this->pubmsg(" ");
				$this->betting_round = $this->game->getBettingRound();
				$cards = $this->game->getBoardCards();
				switch($this->betting_round) {
					case 1:
						$msg = "The flop comes:";
					break;
					case 2:
						$msg = "The turn comes:";
					break;
					case 3:
						$msg = "The river comes:";
					break;
					case 4:
						$this->printShowdown();
						$this->nextRound();
						return;
					case -1:
						$this->printUncontested();
						$this->nextRound();
						return;
					case -2:
						$this->printAllInShowdown();
						$this->nextRound();
						return;
					break;
				}
				foreach($cards as $card) {
					$msg .= " " . $card->getIrcStr();
				}
				$this->pubmsg($msg);
				$this->pubmsg(" ");
				$this->printPot();
				$this->printStatus();
			} else {
				$this->printPot();
			}
			$this->tellAction();
		}

		private function tellAction() {
			$this->privmsg($this->game->getCurrentPlayer()->getName(), "Action is to you.  What would you like to do?");
		}

		private function printShowdown() {
			$this->pubmsg("Showdown");
			$this->pubmsg("--------");
			$this->printShowdownResults();
		}

		private function printAllInShowdown() {
			$this->pubmsg("All-In Showdown");
			$this->pubmsg("---------------");
			$before = $this->game->getBeforeAllInShowdownRound();
			if($before < 3) {
				foreach($this->game->getPlayersInRound() as $p) {
					$this->pubmsg($p->getName() . " has: " . $this->colorCards($p->getCards()));
				}
				$this->pubmsg(" ");
				$cards = $this->game->getBoardCards();
				$flopstr = $cards[0]->getIrcStr() . " " . $cards[1]->getIrcStr() . " " . $cards[2]->getIrcStr();
				$turnstr = $flopstr . " " . $cards[3]->getIrcStr();
				$riverstr = $turnstr . " " . $cards[4]->getIrcStr();
				switch($before) {
					case 0: // flop
						$this->pubmsg("The flop comes: $flopstr");
					case 1: // turn
						$this->pubmsg("The turn comes: $turnstr");
					case 2: // river
						$this->pubmsg("The river comes: $riverstr");
				}
				$this->pubmsg(" ");
			}
			$this->printShowdownResults();
		}

		private function printShowdownResults() {
			$winners = $this->game->getWinners();
			foreach($winners as $name=>$amt) {
				// player1 wins $500 with .. .. (hand)
				$p = $this->findPlayer($name);
				$ret = "$name wins \$$amt with: " . $this->colorCards($p->getCards());
				$hand = $this->game->getHand($p);
				$ret .= " ($hand)";
				$this->pubmsg($ret);
			}

			$muckers = $this->game->getMuckers();
			foreach($muckers as $name) {
				$this->pubmsg("$name mucks");
			}

			// All non-winners and non-muckers show and lose
			$losers = array();
			foreach($this->game->getPlayersInRound() as $p) {
				$losers[$p->getName()] = $p;
			}
			foreach($winners as $name=>$amt) {
				unset($losers[$name]);
			}
			foreach($muckers as $name) {
				unset($losers[$name]);
			}
			foreach($losers as $name=>$p) {
				$hand = $this->game->getHand($p);
				$this->pubmsg("$name had: " . $this->colorCards($p->getCards()) . " ($hand)");
			}
			$this->pubmsg(" ");
		}

		private function printUncontested() {
			$winners = $this->game->getWinners();
			if(count($winners) > 1) throw new Exception("More than one uncontested winner");
			foreach($winners as $name=>$amount) {
				$this->pubmsg("$name wins \$$amount uncontested");
			}
			$this->pubmsg(" ");
		}

		private function printPot() {
			$this->pubmsg($this->game->getPotStr());
		}

		private function parseCommand($player, $text) { // Returns whether or not the command advanced the game
			$nick = $player->getName();
			$text = str_replace("all in", "allin", $text);
			$text = str_replace("make it", "makeit", $text);
			$text = str_replace("$", "", $text);

			$parts = preg_split("/ +/", $text);
			$ret = null;
			$ch = null;
			switch($parts[0]) {
				case 'check':
					$ret = $this->game->check($player);
					if($ret == null) $ch = $nick . " checks";
					break;
				case 'call':
					$ret = $this->game->call($player);
					if($ret == null) $ch = $nick . " calls";
					break;
				case 'allin':
					$ret = $this->game->allIn($player);
					if($ret == null) $ch = $nick . " goes all in";
					break;
				case 'bet':
					$ret = $this->game->bet($player, $parts[1]);
					if($ret == null) $ch = $nick . " bets $" . $parts[1];
					break;
				case 'fold':
					$ret = $this->game->fold($player);
					if($ret == null) $ch = $nick . " folds";
					break;
				case 'raise':
					$ret = $this->game->raise($player, $parts[1]);
					if($ret == null) $ch = $nick . " raises $" . $parts[1];
					break;
				default:
					$ret = "Unrecognized command";
					break;
			}
			if($ret != null) $this->privmsg($player->getName(), $ret);
			if($ch != null) {
				$this->pubmsg($ch);
				return true;
			}
			return false;
		}

		public function beginGame() {
			if(count($this->players) <= 1) {
				$this->pubmsg("Not enough players, aborting game");
				$this->reset();
			} else {
				$this->game_begun = true;
				$this->pubmsg("Registration over, beginning game");
				$this->pubmsg("To play, message me commands: bet <amt>, raise <amt>, call, fold, check, all in");
				$str = "Players:";
				foreach($this->players as $p) {
					$str .= " " . $p->getName();
				}
				$this->pubmsg($str);
				$this->pubmsg(" ");
				$this->game->beginGame();
				$this->round_number = 1;
				$this->betting_round = $this->game->getBettingRound();
				$this->startRound();
			}
		}

		private function startRound() {
			$this->pubmsg("Round " . $this->round_number);
			$this->printStatus();
			$this->pubmsg(" ");
			$this->pubmsg("Dealing cards...");
			foreach($this->players as $p) {
				$this->privmsg($p->getName(), 'You were dealt: ' . $this->colorCards($p->getCards()));
			}
			$this->game->postBlinds();
			$sb = $this->game->getCurrentSB();
			$bb = $this->game->getCurrentBB();
			$this->pubmsg($sb->getName() . " posts \${$this->smallBlind} for the small blind");
			$this->pubmsg($bb->getName() . " posts \${$this->bigBlind} for the big blind");
			$this->tellAction();
		}

		public function nextRound() {
			$this->game->nextRound();
			$this->players = $this->game->getPlayers();
			// If we're out of players
			if(count($this->players) == 1) {
				$this->pubmsg("Game over");
				$this->pubmsg("Winner: " . $this->players[0]->getName());
				$this->reset();
				return;
			}
			$this->round_number++;
			$this->betting_round = $this->game->getBettingRound();
			$this->startRound();
		}

		public function colorCards($cards) {
			$ret = array();
			foreach($cards as $card) {
				$ret[] = $card->getIrcStr();
			}
			return join(" ", $ret);
		}

		public function printStatus() {
			$this->pubmsg("--------------------------");
			$this->pubmsg("Pos   Player         Stack");
			$dealer = $this->game->getCurrentDealer();
			$bb = $this->game->getCurrentBB();
			$sb = $this->game->getCurrentSB();
			foreach($this->players as $player) {
				if($dealer != null && $player->equals($dealer)) {
					if($player->equals($sb)) $pos = "D/SB";
					else $pos = "D";
				} else if($player->equals($bb)) $pos = "BB";
				else if($player->equals($sb)) $pos = "SB";
				$pos = str_pad($pos, 6);
				$name = $player->getName();
				$name = str_pad($name, 15);
				$amt = $player->getMoney();
				$amt = str_pad('$' . $amt, 5, " ", STR_PAD_LEFT);
				$this->pubmsg("$pos$name$amt");
			}
		}

		private function reset() {
			$this->players = array();
			$this->registration_begun = false;
			$this->game_begun = false;
			$this->game = new Game();
			$this->game->setInitialStackSize($this->stacksize);
			$this->game->setBlinds($this->smallBlind, $this->bigBlind);
		}

		private function addPlayer($nick) {
			$player = new Player($nick);
			$this->players[] = $player;
			$this->game->addPlayer($player);
			$this->pubmsg("$nick has registered");
		}

		private function findPlayer($nick) {
			foreach($this->players as $p) {
				if($p->getName() == $nick) return $p;
			}
			return null;
		}

		private function pubmsg($text) {
			call_user_func($this->pub_callback, $text);
		}

		private function privmsg($nick, $text) {
			call_user_func($this->private_callback, $nick, $text);
		}

		public function stackDeck($str) {
			$this->game->stackDeck($str);
		}
	}
?>
