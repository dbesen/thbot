<?

	// This class needs to be rewritten from the ground up.
	// Here are its responsibilities:
	// - Tell you whose turn it is to go
	// - Tell you whether or not the current betting round is over
	// - Tell you who the current dealer, sb, and bb are
	// - Tell you who mucks, given a board? wtf? -- that should be moved to another class
	// It needs to be told:
	// - When a player acts
	// - When the round is over and the dealer should be advanced
	// - When a player folds or goes all in? no! the player knows that ($p->active())
	// - When a player is eliminated (can only happen between rounds)

	// it's possible to have a dead small blind! and/or a dead big blind... fux


	class PlayerOrder {
		private $players;
		private $dealer;
		private $sb;
		private $bb;
		private $acted;
		private $currentPlayer;
		private $blinded;

		public function __construct($players) {
			$this->players = $players;
			$this->dealer = $this->players[0];
			$this->calculateBlindPositions();
			$this->resetActed();
			$this->resetCurrentPlayer();
			$this->blinded = false;
		}

		private function resetActed() {
			$this->acted = array();
			foreach($this->players as $k=>$v) {
				$this->acted[$k] = 0;
			}
		}

		private function resetCurrentPlayer() {
			$this->currentPlayer = $this->getNextPlayer($this->dealer, true, true);
			if(count($this->players) == 2 && !$this->blinded) $this->currentPlayer = $this->dealer;
		}

		public function getCurrentPlayer() {
			// this is needed so we don't have to call acted() for folding
			while(!$this->currentPlayer->active()) {
				$this->currentPlayer = $this->getNextPlayer($this->currentPlayer, true, true);
			}
			return $this->currentPlayer;
		}

		public function isCurrentPlayer(Player $p) { // convenience method...
			if($p->equals($this->getCurrentPlayer())) return true;
			return false;
		}

		public function getDealer() {
			if($this->dealer->eliminated()) return null;
			return $this->dealer;
		}

		public function getSB() {
			if($this->sb->eliminated()) return null;
			return $this->sb;
		}

		public function getBB() {
			if($this->bb->eliminated()) return null;
			return $this->bb;
		}

		public function acted($player, $raised = false) {
			
			if(!$player->equals($this->getCurrentPlayer())) {
				throw new Exception("Wrong player acting; should be {$this->getCurrentPlayer()} but is $player");
			}

			$key = $this->findPlayerKey($player);

			if($raised) {
				$this->resetActed();
			}

			// Increment acted count
			$this->acted[$key]++;
			$this->currentPlayer = $this->getNextPlayer($this->currentPlayer, true, true);
		}

		public function allPlayersActedSinceRaise() {
			$c = -1; // check value
			foreach($this->acted as $k=>$v) {
				$player = $this->players[$k];
				if(!$player->active()) continue;
				if($player->eliminated()) continue;
				if($v < 1) {
					return false;
				}
				if($c == -1) $c = $v;
				if($c != $v) {
					return false;
				}
			}
			return true;
		}

		public function postedBlinds() {
			$this->resetActed();
			$this->blinded = true;
		}

		public function finishBettingRound() {
			$this->resetActed();
			$this->resetCurrentPlayer();
		}

		public function finishRound() { // round as in advance the dealer, not as in current betting round
			// Remove all eliminated players not in an important position
			$newplayers = array();
			$saveNewDealer = null;
			foreach($this->players as $player) {
				$important = false;
				if($player->equals($this->dealer)) {
					$saveNewDealer = $this->getNextPlayer($this->dealer, false, false);
				}
				if($player->equals($this->sb)) $important = true; // will become dead dealer
				if($player->equals($this->bb)) $important = true; // will become dead small
				if(!$important && $player->eliminated()) continue;
				$newplayers[] = $player;
			}
			$this->players = $newplayers;

			// Advance the dealer, sb, bb
			if($saveNewDealer != null) $this->dealer = $saveNewDealer;
			else $this->dealer = $this->getNextPlayer($this->dealer, false, false);
			$this->calculateBlindPositions();

			// Reset acted counts (also deletes removed players from acted)
			$this->blinded = false;
			$this->resetActed();
			$this->resetCurrentPlayer();
		}

		private function calculateBlindPositions() {
			$this->sb = $this->getNextPlayer($this->dealer, false, false);

			// If we're heads-up with no dead dealer/dead sb, swap sb and bb.  Poker is weird.
			if(count($this->players) == 2) {
				$this->sb = $this->dealer;
			}
			$this->bb = $this->getNextPlayer($this->sb, false, false);
		}

		public function removePlayer($player) {
			$player->eliminated(true);
		}

		private function findPlayerKey($player) {
			foreach($this->players as $k=>$v) {
				if($player->equals($v)) return $k;
			}
			throw new Exception("Couldn't find player $player");
		}

		private function getNextPlayer(Player $player, $onlyActive, $onlyNotEliminated) {
			// todo: when this class is done, set defaults for onlyActive and onlyNotEliminated, and remove one if necessary.
			$key = $this->findPlayerKey($player);
			$c = 0;
			do {
				$c++;
				if($c > count($this->players)) throw new Exception("Couldn't find next player");
				$key++;
				$key %= count($this->players);
				$active = $this->players[$key]->active();
				$eliminated = $this->players[$key]->eliminated();
				if($onlyActive == false) $active = true;
				if($onlyNotEliminated == false) $eliminated = false;
			} while(!$active || $eliminated);
			return $this->players[$key];
		}

		public function getMuckers($board) {
			// Remember if you're all in you're removed from the player order (and never muck)
			//get a count of activep layers
			$c = 0;
			foreach($this->players as $p) {
				if($p->eliminated()) continue;
				if(!$p->active()) continue;
				$c++;
			}
			if($c <= 1) return array();
			$currentPlayer = $this->currentPlayer;
			$firstPlayer = $currentPlayer;
			$bestHand = $board->getHand($firstPlayer);
			$ret = array();
			do {
				$currentHand = $board->getHand($currentPlayer);
				if($currentHand->compareTo($bestHand) < 0) {
					$ret[] = $currentPlayer->getName();
				} else {
					$bestHand = $currentHand;
				}
				$currentPlayer = $this->getNextPlayer($currentPlayer, true, true);
			} while(!$firstPlayer->equals($currentPlayer));

			return $ret;
		}
	}
?>
