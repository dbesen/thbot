<?
	class Pot {
		private $players = array();
		private $playersPutIn = array();
		private $cutoffPoints = array();
		private $removedPlayers = array();
		private $paid = false;

		public function addMoney($player, $amt) {
			if($amt == 0) return;
			$this->doTransfer($player, $amt);
			if($player->getMoney() == 0) {
				$this->cutoffPoints[] = $this->playersPutIn[$player->getname()];
			}
		}

		private function doTransfer($player, $amt) {
			$player->addMoney(0 - $amt);
			$this->players[$player->getName()] = $player;
			$this->playersPutIn[$player->getName()] += $amt;
		}

		public function removePlayer($player) {
			$this->removedPlayers[$player->getName()] = true;
		}

		private function getStakes() {
			// todo: make this non-destructive
			// For each cutoff point, we need a set of players and an amount
			$cutoffPoints = $this->cutoffPoints;
			$playersPutIn = $this->playersPutIn;

			sort($cutoffPoints);
			$cutoffPoints[] = null; // infinity
			$lowerBy = 0;
			$ret = array();
			foreach($cutoffPoints as $cutoffPoint) {
				$cutoffPoint -= $lowerBy;
				$currPlayers = array();
				$amt = 0;
				foreach($this->players as $player) {
					$playerPutIn = $playersPutIn[$player->getName()];
					if($playerPutIn <= 0) continue;

					if(!$this->removedPlayers[$player->getName()])
						$currPlayers[] = $player;

					// See how much money is involved
					if($cutoffPoint == null || $playerPutIn <= $cutoffPoint) {
						$amt += $playerPutIn;
						$playersPutIn[$player->getName()] = 0;
					} else {
						$amt += $cutoffPoint;
						$playersPutIn[$player->getName()] -= $cutoffPoint;
					}
				}
				if($amt != 0) {
					$r['players'] = $currPlayers;
					$r['amount'] = $amt;
					$ret[] = $r;
				}
				$lowerBy += $cutoffPoint;
			}
			return $ret;
		}

		public function payout($board) {
			if($this->paid == true) throw new Exception("Pot::payout called twice");
			$this->paid = true;
			$stakes = $this->getStakes();
			foreach($stakes as $stake) {
				$this->distributeMoney($board, $stake['players'], $stake['amount']);
			}
		}

		private function distributeMoney($board, $players, $amount) {
			if(count($players) == 0) {
				if($amount == 0) return;
				throw new Exception("Pot cutoff with no players");
			}
			if(count($players) == 1) {
				foreach($players as $player) {
					$player->addMoney($amount);
					$amount = 0;
					return;
				}
			}

			$bestHand = null;
			foreach($players as $player) {
				$hand = $board->getHand($player);
				if($bestHand == null || $bestHand->compareTo($hand) < 0) {
					$bestHand = $hand;
				}
			}

			$bestPlayers = array();
			foreach($players as $player) {
				$hand = $board->getHand($player);
				if($hand->compareTo($bestHand) == 0) $bestPlayers[] = $player;
			}

			$payout = (int) ($amount / count($bestPlayers));
			foreach($bestPlayers as $player) {
				$player->addMoney($payout);
			}

			$amount -= ($payout * count($bestPlayers));

			$bestPlayers = array_reverse($bestPlayers);
			foreach($bestPlayers as $player) {
				if($amount != 0) {
						$player->addMoney(1);
						$amount -= 1;
				}
			}
		}

		public function __toString() {
			$stakes = $this->getStakes();
			if(count($stakes) == 1) {
				return "Main pot: $" . $stakes[0]['amount'];
			}

			$ret = "Pots: ";
			$arr = array();
			foreach($stakes as $stake) {
				$tmp = '$' . $stake['amount'] . " (";
				$names = array();
				foreach($stake['players'] as $p) {
					$names[] = $p->getName();
				}
				sort($names);
				$tmp .= join(", ", $names) . ')';
				$arr[] = $tmp;
			}
			$ret .= join(", ", $arr);
			return $ret;
		}
	}
?>
