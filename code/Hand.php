<?
	class Hand {
		private $cards = array();
		private $description;
		private $handType;
		private $kickers = array();

		// 1: High card
		// 2: Pair
		// 3: Two pair
		// 4: Three of a kind
		// 5: Straight
		// 6: Flush
		// 7: Full house
		// 8: Four of a kind
		// 9: Straight flush

		public function __construct() {
			$args = func_get_args();
			foreach($args as $card) {
				if(is_array($card)) {
					foreach($card as $c) {
						if($c instanceof Card) $this->cards[] = $c;
						else $this->cards[] = new Card($c);
					}
				} else {
					if($card instanceof Card) $this->cards[] = $card;
					else $this->cards[] = new Card($card);
				}
			}

			$c = count($this->cards);
			if($c < 5) throw new Exception("Not enough cards");

			usort($this->cards, array($this, "cardSort"));
			$this->cards = array_reverse($this->cards); // Descending order

			$this->handType = $this->identifyHand();
		}

		private function cardSort($a, $b) {
			return $a->compareTo($b);
		}

		private function identifyHand() {

			if($this->isStraightFlush()) return 9;
			if($this->isFourOfAKind()) return 8;
			if($this->isFullHouse()) return 7;
			if($this->isFlush()) return 6;
			if($this->isStraight()) return 5;
			if($this->isThreeOfAKind()) return 4;
			if($this->isTwoPair()) return 3;
			if($this->isPair()) return 2;

			$this->description = $this->getHighCardStr();
			return 1;
		}

		private function isStraightFlush() {
			// The flush function removes the cards that are not part of the flush
			// so the straight function can find any straight
			if($this->isFlush() && $this->isStraight()) {
				if($this->straightStr == "Ten to ace")
					$this->description = "Royal flush";
				else
					$this->description = "Straight flush: " . $this->straightStr;
				return true;
			}
			return false;
		}

		private function isFourOfAKind() {
			$ranks = array();
			foreach($this->cards as $card) {
				$ranks[$card->rankStr()][] = $card;
			}

			$kicker = null;
			$got_four = false;
			foreach($ranks as $rank=>$cards) {
				if(count($cards) == 4) {
					if($rank == "Six") $rank = "Sixe";
					$this->description = "Four of a kind: " . $rank . "s";
					$got_four = true;
					$this->kickers = $cards;
				} else if($kicker == null) $kicker = $cards[0];
			}

			if($got_four) {
				$this->kickers[] = $kicker;
				return true;
			}
			return false;
		}

		private function isFullHouse() {
			$ranks = array();
			foreach($this->cards as $card) {
				$ranks[$card->rankStr()][] = $card;
			}

			$threes = array();
			$twos = array();
			foreach($ranks as $rank=>$cards) {
				if(count($cards) == 3) {
					$threes[] = $cards;
				}
				if(count($cards) == 2) {
					$twos[] = $cards;
				}
			}

			if(count($threes) == 0) return false;

			$first = $threes[0][0]->rankStr();
			$first_kicker = $threes[0][0];

			// Excluding that, find the highest pair
			array_shift($threes);

			if(count($threes) >= 1) {
				foreach($threes as $a) { // there can only be one element
					$second = $a[0]->rankStr();
					$second_kicker = $a[0];
				}
			} else {

				if(count($twos) == 0) return false;

				// Find the highest pair
				$second = $twos[0][0]->rankStr();
				$second_kicker = $twos[0][0];
			}

			if($first == "Six") $first = "Sixe";
			if($second == "Six") $second = "Sixe";
			$second = strtolower($second);
			$this->description = "Full house: {$first}s full of {$second}s";
			$this->kickers = array($first_kicker, $second_kicker);
			return true;

		}

		private function isFlush() {
			// This removes the cards that are not part of the flush
			$suits = array();
			foreach($this->cards as $card) {
				$suits[$card->suitStr()]++;
			}

			$flushSuit = null;
			foreach($suits as $suit=>$count) {
				if($count >= 5) {
					$this->description = "Flush: " . $suit;
					$flushSuit = $suit;
				}
			}

			if($flushSuit != null) {
				foreach($this->cards as $key=>$card) {
					if($card->suitStr() != $flushSuit)
						$this->cards[$key] = null;
				}

				$new_cards = array();
				foreach($this->cards as $card) {
					if($card == null) continue;
					$new_cards[] = $card;
				}
				$this->cards = $new_cards;

				$this->kickers = array_slice($this->cards, 0, 5);
				return true;
			}
			return false;
		}

		private function isStraight() {
			// This assumes the cards are in descending order of rank
			$prevCard = null;
			$count = 1;
			$straightCards = array();
			foreach($this->cards as $card) {
				if($prevCard != null) {
					$c = $prevCard->compareTo($card);
					if($c == 1) {
						$count++;
						$straightCards[] = $card;
						if($count == 5) break;
					} else if($c != 0) {
						$count = 1;
						$straightCards = array($card);
					}
				} else {
					$straightCards = array($card);
				}
				$prevCard = $card;
			}
			// A 5 4 3 2 or A 7 5 4 3 2 or A 7 5 4 3 2 2
			if($count == 4 && $straightCards[3]->rankStr() == "Two" && $this->cards[0]->rankStr() == "Ace") {
				$count++;
				array_push($straightCards, $this->cards[0]);
			}
			if($count == 5) {
				$this->straightStr = $straightCards[4]->rankStr() . " to " . strtolower($straightCards[0]->rankStr());
				$this->description = "Straight: " . $this->straightStr;
				$this->kickers = $straightCards;
				return true;
			}
			return false;
		}

		private function isThreeOfAKind() {
			$ranks = array();
			foreach($this->cards as $card) {
				$ranks[$card->rankStr()][] = $card;
			}

			$others = array();
			$got_three = false;
			foreach($ranks as $rank=>$cards) {
				if(count($cards) == 3) {
					// If there's more than one set of three, we have a full house
					if($rank == "Six") $rank = "Sixe";
					$this->description = "Three of a kind: " . $rank . "s";
					$this->kickers = $cards;
					$got_three = true;
				} else {
					$others[] = $cards[0];
				}
			}
			if($got_three) {
				$this->kickers[] = $others[0];
				$this->kickers[] = $others[1];
				return true;
			}
			return false;
		}

		private function isTwoPair() {
			$ranks = array();
			foreach($this->cards as $card) {
				$ranks[$card->rankStr()][] = $card;
			}

			$twos = array();
			$kicker = null;
			foreach($ranks as $rank=>$cards) {
				if(count($cards) == 2) {
					$twos[] = $cards;
				} else if($kicker == null) $kicker = $cards[0];
			}

			if(count($twos) >= 2) {
				// They're already in descending order
				$first = $twos[0][0]->rankStr();
				$second = $twos[1][0]->rankStr();
				if($first == "Six") $first = "Sixe";
				if($second == "Six") $second = "Sixe";
				$second = strtolower($second);
				$this->description = "Two pair: {$first}s and {$second}s";

				$this->kickers = array_merge($twos[0], $twos[1]);
				$this->kickers[] = $kicker;
				return true;
			}

			return false;
		}

		private function isPair() {
			$ranks = array();
			foreach($this->cards as $card) {
				$ranks[$card->rankStr()][] = $card;
			}

			$other = array();
			$found_pair = false;
			foreach($ranks as $rank=>$cards) {
				if(count($cards) == 2) {
					if($rank == "Six") $rank = "Sixe";
					$this->description = "Pair: " . $rank . "s";
					// The first 2 kickers are the pair
					$this->kickers = $cards;
					$found_pair = true;
				} else {
					// The next 3 are the first 3 non-pair cards
					$other[] = $cards[0]; // there can only be one card
				}
			}
			if($found_pair) {
				$this->kickers[] = $other[0];
				$this->kickers[] = $other[1];
				$this->kickers[] = $other[2];
				return true;
			}
			return false;
		}

		private function getHighCardStr() {
			$bestcard = null;
			foreach($this->cards as $card) {
				if($bestcard == null || $bestcard->compareTo($card) < 0) {
					$bestcard = $card;
				}
			}

			$this->kickers = array_slice($this->cards, 0, 5);

			return "High card: " . $bestcard->rankStr();
		}

		public function __toString() {
			return $this->description;
		}

		public function compareTo(Hand $h) {
			if($this->handType != $h->handType) return $this->handType - $h->handType;

			foreach($this->kickers as $k=>$v) {
				$c = $v->compareTo($h->kickers[$k]);
				if($c != 0) return $c;
			}
			return 0;
		}
	}
?>
