<?
	class Game {
		private $players = array();
		private $initialStackSize = 500;
		private $bigBlind = 10;
		private $smallBlind = 5;
		private $gameStarted = false;
		private $pot;
		private $toGo;
		private $minRaise;
		private $currentBets;
		private $board;
		private $deck;
		private $bettingRound;
		private $playerOrder;
		private $winners;
		private $muckers;
		private $beforeAllInRound;

		public function __construct() {
			$this->board = new Board();
			$this->deck = new Deck();
		}

		public function addPlayer(Player $p) {
			if($this->gameStarted == true) {
				throw new Exception("The game has already started");
			}
			$this->players[] = $p;
		}

		public function setInitialStackSize($amt) {
			$this->initialStackSize = $amt;
		}

		public function setBlinds($small, $big) {
			$this->bigBlind = $big;
			$this->smallBlind = $small;
		}

		public function beginGame() {
			if($this->gameStarted == true) {
				throw new Exception("The game has already started");
			}
			$this->gameStarted = true;
			foreach($this->players as $player) {
				$player->addMoney($this->initialStackSize);
			}
			$this->playerOrder = new PlayerOrder($this->players);

			$this->beginRound();
		}

		private function beginRound() {
			$this->currentBets = array();
			$this->deck->shuffle();
			$this->board->clearCards();
			$this->pot = new Pot();

			$this->winners = null;
			$this->muckers = null;

			$this->bettingRound = 0;
			$this->toGo = 0;

			$this->beforeAllInRound = 0;

			foreach($this->players as $p) {
				$p->folded(false);
				$p->allIn(false);
			}

			$this->deal();
		}

		private function endRound() {
			// Determine winners
			$playerDeltas = array();
			foreach($this->players as $p) {
				$playerDeltas[$p->getName()] = -1 * $p->getMoney();
			}
			$this->pot->payout($this->board);
			$this->winners = array();
			foreach($this->players as $p) {
				$playerDeltas[$p->getName()] += $p->getMoney();
				if($playerDeltas[$p->getName()] < 0) throw new Exception("Negative player delta");
				if($playerDeltas[$p->getName()] > 0) $this->winners[$p->getName()] = $playerDeltas[$p->getName()];
			}

			// You're a mucker if a better hand has been shown before you in the player order
			$this->muckers = $this->playerOrder->getMuckers($this->board);
		}

		public function nextRound() {
			// Remove players that were eliminated last round
			foreach($this->players as $k=>$player) {
				if($player->getMoney() == 0) {
					// Remove the player from the game
					unset($this->players[$k]);
					// Remove the player from the player order
					$this->playerOrder->removePlayer($player);
				}
			}

			$this->winners = null;
			$this->muckers = null;

			$this->beginRound();
			$this->playerOrder->finishRound();
		}

		private function deal() {
			foreach($this->players as $player) {
				$player->clearCards();
				$player->addCard($this->deck->dealCard());
				$player->addCard($this->deck->dealCard());
			}
		}

		public function postBlinds() {
			$this->minRaise = 0; // so we can post the min blind as a raise
			$res = "";
			$currentSB = $this->playerOrder->getSB();
			if($currentSB != null) {
				if($currentSB->getMoney() < $this->smallBlind) {
					$res .= $this->bet($currentSB, $currentSB->getMoney());
				} else {
					$res .= $this->bet($currentSB, $this->smallBlind);
				}
			}

			$currentBB = $this->playerOrder->getBB();
			if($currentBB != null) {
				if($currentBB->getMoney() < $this->bigBlind) {
					$res .= $this->bet($currentBB, $currentBB->getMoney());
					$this->toGo = $this->bigBlind;
				} else {
					$res .= $this->bet($currentBB, $this->bigBlind);
				}
			}

			$this->minRaise = $this->bigBlind;
			if($res != "") throw new Exception("postBlinds: res is $res");
			$this->playerOrder->postedBlinds();
		}

		public function getCurrentSB() {
			return $this->playerOrder->getSB();
		}

		public function getCurrentBB() {
			return $this->playerOrder->getBB();
		}

		public function getCurrentDealer() {
			return $this->playerOrder->getDealer();
		}

		public function getCurrentPlayer() {
			return $this->playerOrder->getCurrentPlayer();
		}

		public function bet(Player $player, $amt) {
			if($amt === null || $amt === "") return "Bet needs an amount";
			if($amt == 0) return "You can't bet nothing!";
			if($amt < 0) return "You can't bet a negative amount!";

			return $this->do_bet($player, $amt);
		}

		private function do_bet(Player $player, $amt) {
			if(!in_array($player, $this->players)) return "You are not in the game";
			if(!$this->playerOrder->isCurrentPlayer($player)) return "It's not your turn";
			if($amt > $player->getMoney()) return "You don't have enough money for that";

			// % doesn't do decimals, so we do this
			$m = $amt / $this->smallBlind;
			if($m !== (int) $m) return "Bets must be a multiple of the small blind";

			$allIn = false;
			if(($player->getMoney() == $amt) && ($amt > 0))
				$allIn = true;

			$playersToGo = $this->toGo - $this->currentBets[$player->getName()];
			if(!$allIn && $amt < $playersToGo) {
				return "That's not enough to call";
			}

			$raised = false;
			if($amt > $playersToGo) { // raise
				// The minimum raise is equal to the largest bet so far

				if(!$allIn) {
					if($amt < ($playersToGo + $this->minRaise)) return 'The minimum raise is $' . $this->minRaise;
					$this->minRaise = $amt - $playersToGo;
				}
				$raised = true;
			}
			$this->currentBets[$player->getName()] += $amt;
			$this->toGo = $this->currentBets[$player->getName()];

			$this->pot->addMoney($player, $amt);

			$this->playerOrder->acted($player, $raised);

			// Set them to all in after they act; if we do it before, the action will fail since the player isn't active
			if($allIn) {
				$player->allIn(true);
			}

			$this->checkEndOfRound();
		}

		private function checkEndOfRound() {

			// If all but one have folded
			$nfc = 0; // non-fold count
			foreach($this->players as $p) {
				if(!$p->folded()) $nfc++;
			}
			if($nfc == 1) {
				$this->bettingRound = -1;
				$this->endRound();
				return;
			}

			// Have all active, non-eliminated players acted since the last raise?
			if($this->playerOrder->allPlayersActedSinceRaise()) {
				// If every player is inactive or eliminated
				$count = 0;
				$all_done = true;
				foreach($this->players  as $player) {
					if($player->eliminated()) continue;
					if($player->active()) $count++;
				}
				if($count > 1) $all_done = false;
				if($all_done) { // If all have folded or gone all in
					$this->allInShowdown();
					return;
				}
				$this->advanceBettingRound();
			}
		}

		private function allInShowdown() {
			switch($this->bettingRound) {
				case 0: // flop
					$this->board->addCard($this->deck->dealCard());
					$this->board->addCard($this->deck->dealCard());
					$this->board->addCard($this->deck->dealCard());
				case 1: // Turn
					$this->board->addCard($this->deck->dealCard());
				case 2: // River
					$this->board->addCard($this->deck->dealCard());
				case 3: // Showdown
					$this->beforeAllInRound = $this->bettingRound;
					$this->bettingRound = -2;
					$this->endRound();
			}
		}

		public function getBeforeAllInShowdownRound() {
			return $this->beforeAllInRound;
		}

		private function advanceBettingRound() {
			$this->bettingRound++;
			switch($this->bettingRound) {
				case 1: // flop
					$this->board->addCard($this->deck->dealCard());
					$this->board->addCard($this->deck->dealCard());
					$this->board->addCard($this->deck->dealCard());
					break;
				case 2: // Turn
					$this->board->addCard($this->deck->dealCard());
					break;
				case 3: // River
					$this->board->addCard($this->deck->dealCard());
					break;
				case 4: // Showdown
					$this->endRound();
					return;
					break;
			}
			$this->playerOrder->finishBettingRound();
			$this->minRaise = $this->bigBlind;
		}

		public function call(Player $player) {
			$amt = $this->toGo - $this->currentBets[$player->getname()];
			if($amt == 0) return "You can't call now -- did you mean check?";
			if($amt > $player->getMoney()) $amt = $player->getMoney();
			return $this->do_bet($player, $amt);
		}

		public function raise(Player $player, $amt) {
			if($amt === null || $amt === "") return "Raise needs an amount";
			if($amt == 0) return "You can't raise nothing!";
			if($amt < 0) return "You can't raise a negative amount!";
			return $this->do_bet($player, $this->toGo - $this->currentBets[$player->getName()] + $amt);
		}

		public function allIn(Player $player) {
			$ret = $this->do_bet($player, $player->getMoney());
			return $ret;
		}

		public function check(Player $player) {
			return $this->do_bet($player, 0);
		}

		public function fold(Player $player) {
			if(!in_array($player, $this->players)) return "You are not in the game";
			if(!$this->playerOrder->isCurrentPlayer($player)) return "It's not your turn";

			$this->pot->removePlayer($player);

			$player->folded(true);

			$this->checkEndOfRound();
		}

		public function getBoardStr() {
			return $this->board->__toString();
		}

		public function stackDeck($s) { // for testing
			$this->deck->stack($s);
		}

		public function getBettingRound() {
			return $this->bettingRound;
		}

		public function getBoardCards() {
			return $this->board->getCards();
		}

		public function getHand($player) {
			return new Hand($this->board->getCards(), $player->getCards());
		}

		public function getPotStr() {
			return $this->pot->__toString();
		}

		public function getWinners() {
			return $this->winners;
		}

		public function getMuckers() {
			return $this->muckers;
		}

		public function getPlayers() {
			$ret = array();
			foreach($this->players as $p) {
				$ret[] = $p;
			}
			return $ret;
		}

		public function getPlayersInRound() { // all who haven't folded
			$ret = array();
			foreach($this->players as $p) {
				if($p->folded()) continue;
				$ret[] = $p;
			}
			return $ret;
		}
	}
?>
