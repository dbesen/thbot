<?
	require_once('Net/SmartIRC.php');

	class Colorbot {
		private $channel = '#test';
		private $irc;

		function __construct() {
			$this->irc = new Net_SmartIRC();

			//$this->irc->setDebug(SMARTIRC_DEBUG_ALL);
			$this->irc->setUseSockets(TRUE);
			$this->irc->setSenddelay(0);
			$this->irc->connect('localhost', 6667);
			$this->irc->login('colorbot', 'colorbot', 0, 'colorbot');
			$this->irc->join($this->channel);
			$this->irc->registerActionhandler(SMARTIRC_TYPE_CHANNEL, '!colors', $this, 'draw');
			$this->irc->registerActionhandler(SMARTIRC_TYPE_CHANNEL, '!mem', $this, 'mem');
			$this->irc->listen();
			$this->irc->disconnect();
		}

		function draw() {
			for($i=0;$i<=16;$i++) {
				$ii = "" . $i;
				if($i < 10) $ii = "0" . $i;
				$line = "";
				$line .= $this->color($ii, $i);
				for($j=0;$j<=16;$j++) {
					$jj = "" . $j;
					if($j < 10) $jj = "0". $j;
					$line .= $this->color("$ii,$jj", $i, $j);
				}
				$this->pubmsg($line);
			}
		}

		function mem() {
			$this->pubmsg("Using " . number_format(memory_get_usage()) . " bytes");
		}

		function color($text, $c1, $c2 = null) {
			if($c2 === null) return "$c1 $text";
			return "$c1,$c2 $text";
		}

		function pubmsg($text) {
			$this->irc->message(SMARTIRC_TYPE_CHANNEL, $this->channel, $text);
		}
	}

	$c = new colorbot();
?>
