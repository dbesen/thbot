<?
	require_once 'PHPUnit/Framework.php';

	require_once("../code/autoloader.php");

	class DeckTest extends PHPUnit_Framework_TestCase {
		public function testAllCards() {
			$deck = new Deck();
			$seen_cards = array();
			for($i=0;$i<52;$i++) {
				$card = $deck->dealCard();
				$s = $card->__toString();
				$this->assertTrue(!isset($seen_cards[$s]));
				$seen_cards[$s] = true;
				$this->assertEquals(get_class($card), "Card");
			}
			$this->assertEquals(null, $deck->dealCard());

			$deck->shuffle();

			$seen_cards = array();
			for($i=0;$i<52;$i++) {
				$card = $deck->dealCard();
				$s = $card->__toString();
				$this->assertTrue(!isset($seen_cards[$s]));
				$seen_cards[$s] = true;
				$this->assertEquals(get_class($card), "Card");
			}
			$this->assertEquals(null, $deck->dealCard());
		}
	}
?>
