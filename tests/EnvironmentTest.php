<?
	require_once 'PHPUnit/Framework.php';

	class EnvironmentTest extends PHPUnit_Framework_TestCase {
		public function testIncludes() {
			$this->assertTrue((boolean)$this->file_exists_incpath("Net/SmartIRC.php"), "Test that SmartIRC is installed");
		}

		private function file_exists_incpath ($file)
		{
			$paths = explode(PATH_SEPARATOR, get_include_path());

			foreach ($paths as $path) {
				// Formulate the absolute path
				$fullpath = $path . DIRECTORY_SEPARATOR . $file;

				// Check it
				if (file_exists($fullpath)) {
					return $fullpath;
				}
			}

			return false;
		}
	}
?>
