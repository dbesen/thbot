<?
	require_once 'PHPUnit/Framework.php';

	require_once("../code/autoloader.php");

	class PotTest extends PHPUnit_Framework_TestCase {
		public function testConstructor() {
			$pot = new Pot();
			$this->assertEquals("Pot", get_class($pot));
		}

		public function testNoPlayers() {
			$pot = new Pot();
			$b = new Board();
			$b->addCard(new Card("Ah"));
			$b->addCard(new Card("Ad"));
			$b->addCard(new Card("As"));
			$pot->payout($b);

			$pot = new Pot();
			$p1 = new Player("Player 1");
			$p1->addMoney(5);
			$pot->addMoney($p1, 5);
			$pot->removePlayer($p1);

			$e = false;
			try {
				$pot->payout($b);
			} catch(Exception $e) {
				$e = true;
			}
			$this->assertEquals(true, $e);
		}

		public function testOnePlayer() {
			$pot = new Pot();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");

			$p1->addMoney(100);
			$p2->addMoney(100);
			$pot->addMoney($p1, 5);
			$pot->addMoney($p2, 5);

			$pot->removePlayer($p2);

			$this->assertEquals(95, $p1->getMoney());
			$this->assertEquals(95, $p2->getMoney());

			$pot->payout(new Board);

			$this->assertEquals(105, $p1->getMoney());
			$this->assertEquals(95, $p2->getMoney());
		}

		public function testPayout() {
			$pot = new Pot();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");

			$p1->addMoney(100);
			$p2->addMoney(100);
			$p3->addMoney(100);

			$pot->addMoney($p1, 5);
			$pot->addMoney($p2, 5);
			$pot->addMoney($p3, 5);

			$pot->removePlayer($p1);

			$pot->addMoney($p2, 5);
			$pot->addMoney($p3, 5);

			$p2->addCard(new Card("Ah"));
			$p2->addCard(new Card("Th"));

			$p3->addCard(new Card("3h"));
			$p3->addCard(new Card("4h"));

			$b = new Board();
			$b->addCard(new Card("Ad"));
			$b->addCard(new Card("3d"));
			$b->addCard(new Card("9d"));

			$pot->payout($b);

			$this->assertEquals(95, $p1->getMoney());
			$this->assertEquals(115, $p2->getMoney());
			$this->assertEquals(90, $p3->getMoney());
		}

		public function testSplitPot() {
			$pot = new Pot();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");

			$p1->addMoney(100);
			$p2->addMoney(100);
			$p3->addMoney(100);

			$pot->addMoney($p1, 5);
			$pot->addMoney($p2, 5);
			$pot->addMoney($p3, 5);

			$pot->removePlayer($p3);

			$p1->addCard(new Card("2h"));
			$p1->addCard(new Card("3h"));
			$p2->addCard(new Card("2d"));
			$p2->addCard(new Card("3d"));

			$b = new Board();
			$b->addCard(new Card("Ad"));
			$b->addCard(new Card("Kd"));
			$b->addCard(new Card("Qd"));
			$b->addCard(new Card("Jd"));
			$b->addCard(new Card("9d"));

			$pot->payout($b);

			$this->assertEquals(102, $p1->getMoney());
			$this->assertEquals(103, $p2->getMoney());
			$this->assertEquals(95, $p3->getMoney());
		}

		public function testCap() {
			$pot = new Pot();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");

			$p1->addMoney(100);
			$p2->addMoney(200);
			$p3->addMoney(300);

			$pot->addMoney($p1, 100);
			$pot->addMoney($p2, 200);
			$pot->addMoney($p3, 300);

			$p1->addCard(new Card("Ah"));
			$p1->addCard(new Card("Ad"));

			$p2->addCard(new Card("Kh"));
			$p2->addCard(new Card("Kd"));

			$p3->addCard(new Card("3c"));
			$p3->addCard(new Card("4h"));

			$b = new Board();
			$b->addCard(new Card("Ad"));
			$b->addCard(new Card("Kd"));
			$b->addCard(new Card("6s"));

			$pot->payout($b);

			$this->assertEquals(300, $p1->getMoney());
			$this->assertEquals(200, $p2->getMoney());
			$this->assertEquals(100, $p3->getMoney());
		}

		public function testCap2() {
			$pot = new Pot();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");

			$p1->addMoney(300);
			$p2->addMoney(200);
			$p3->addMoney(100);

			$pot->addMoney($p1, 300);
			$pot->addMoney($p2, 200);
			$pot->addMoney($p3, 100);

			$p3->addCard(new Card("Ah"));
			$p3->addCard(new Card("Ad"));

			$p2->addCard(new Card("Kh"));
			$p2->addCard(new Card("Kd"));

			$p1->addCard(new Card("3c"));
			$p1->addCard(new Card("4h"));

			$b = new Board();
			$b->addCard(new Card("Ad"));
			$b->addCard(new Card("Kd"));
			$b->addCard(new Card("6s"));

			$this->assertEquals("Pots: $300 (Player 1, Player 2, Player 3), $200 (Player 1, Player 2), $100 (Player 1)", $pot->__toString());

			$pot->payout($b);

			$this->assertEquals(100, $p1->getMoney());
			$this->assertEquals(200, $p2->getMoney());
			$this->assertEquals(300, $p3->getMoney());
		}

		public function testToStr() {
			$pot = new Pot();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");

			$p1->addMoney(100);
			$p2->addMoney(100);

			$pot->addMoney($p1, 100);
			$pot->addMoney($p2, 100);

			$this->assertEquals("Main pot: $200", $pot->__toString());
		}
	}
?>
