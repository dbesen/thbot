<?
	require_once 'PHPUnit/Framework.php';

	require_once("../code/autoloader.php");

	class BoardTest extends PHPUnit_Framework_TestCase {
		public function testConstructor() {
			$board = new Board();
			$this->assertEquals("Board", get_class($board));
		}

		public function testAddCard() {
			$b = new Board();
			$b->addCard(new Card("Ah"));

			$cards = $b->getCards();
			$this->assertEquals($cards[0]->__toString(), "Ah");

			$b->addCard(new Card("Jd"));
			$cards = $b->getCards();
			$this->assertEquals($cards[0]->__toString(), "Ah");
			$this->assertEquals($cards[1]->__toString(), "Jd");

			$b->clearCards();
			$this->assertEquals(0, count($b->getCards()));
		}

		public function testGetHand() {
			$b = new Board();
			$b->addCard(new Card("As"));
			$b->addCard(new Card("Js"));
			$b->addCard(new Card("Ts"));
			$b->addCard(new Card("3c"));
			$b->addCard(new Card("4d"));

			$p = new Player("asfd");
			$p->addCard(new Card("Ac"));
			$p->addCard(new Card("3d"));

			$h = $b->getHand($p);

			$this->assertEquals("Two pair: Aces and threes", $h->__toString());
		}

		public function testToString() {
			$b = new Board();
			$b->addCard(new Card("As"));
			$b->addCard(new Card("Js"));
			$b->addCard(new Card("Ts"));

			$this->assertEquals("As Js Ts", $b->__toString());

			$b->addCard(new Card("3c"));
			$b->addCard(new Card("4d"));

			$this->assertEquals("As Js Ts 3c 4d", $b->__toString());
		}

	}
?>
