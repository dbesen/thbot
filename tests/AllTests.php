<?
	require_once("PHPUnit/Framework.php");
	require_once("PHPUnit/TextUI/TestRunner.php");
	chdir(dirname(__FILE__));
	require_once("../code/autoloader.php");

	$suite = new PHPUnit_Framework_TestSuite();
	$files = glob("*Test.php");
	$suite->addTestFiles($files);

//	$c = $suite->testCount();

	PHPUnit_TextUI_TestRunner::run($suite);
?>
