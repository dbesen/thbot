<?
	require_once 'PHPUnit/Framework.php';

	require_once("../code/autoloader.php");

	class HandTest extends PHPUnit_Framework_TestCase {

		public function testConstructor() {
			$hand = new Hand("Ah", "Ad", "Ac", "As", "3h");
			$this->assertEquals("Hand", get_class($hand));

			$hand = new Hand("Ah", "Ad", "Ac", "As", new Card("3h"));
			$this->assertEquals("Hand", get_class($hand));

			$hand = new Hand("Ah", "Ad", "Ac", "As", "3h", "4c", "5c");
			$this->assertEquals("Hand", get_class($hand));

			$hand = new Hand(array("Ah", "Ad", "Ac", "As", "3h", "4c", "5c"));
			$this->assertEquals("Hand", get_class($hand));

			$hand = new Hand(array("Ah", "Ad"), array("Ac", "As", "3h", "4c", "5c"));
			$this->assertEquals("Hand", get_class($hand));

			// The constructor takes 5 or more cards
			$e = false;
			try {
				$hand = new Hand("Ah", "Ad", "Ac", "As");
			} catch (Exception $e) {
				$e = true;
			}
			$this->assertEquals(true, $e);
		}

		public function testHighCard() {
			$hand = new Hand("Ah", "3d", "4c", "8s", "Td");
			$this->assertEquals("High card: Ace", $hand->__toString());

			$hand = new Hand("2h", "3d", "4c", "8s", "Td");
			$this->assertEquals("High card: Ten", $hand->__toString());
		}

		public function testPair() {
			$hand = new Hand("Ah", "Ad", "2h", "3d", "8c");
			$this->assertEquals("Pair: Aces", $hand->__toString());

			$hand = new Hand("Ah", "Td", "2h", "2d", "8c");
			$this->assertEquals("Pair: Twos", $hand->__toString());

			$hand = new Hand("6h", "6d", "2h", "3h", "8d");
			$this->assertEquals("Pair: Sixes", $hand->__toString());

		}

		public function testTwoPair() {
			$hand = new Hand("Ah", "Ad", "2h", "2d", "8c");
			$this->assertEquals("Two pair: Aces and twos", $hand->__toString());

			$hand = new Hand("Th", "Td", "3h", "3d", "8c");
			$this->assertEquals("Two pair: Tens and threes", $hand->__toString());

			$hand = new Hand("3h", "3d", "Th", "Td", "8c");
			$this->assertEquals("Two pair: Tens and threes", $hand->__toString());

			$hand = new Hand("3h", "3d", "Th", "Td", "8c", "8d");
			$this->assertEquals("Two pair: Tens and eights", $hand->__toString());

			$hand = new Hand("3h", "3d", "Th", "Td", "6c", "6d");
			$this->assertEquals("Two pair: Tens and sixes", $hand->__toString());

			$hand = new Hand("3h", "3d", "6h", "6d", "8c", "9d");
			$this->assertEquals("Two pair: Sixes and threes", $hand->__toString());
		}

		public function testThreeOfAKind() {
			$hand = new Hand("Ah", "Ad", "Ac", "2d", "8c");
			$this->assertEquals("Three of a kind: Aces", $hand->__toString());

			$hand = new Hand("Ah", "2d", "2c", "2s", "8c");
			$this->assertEquals("Three of a kind: Twos", $hand->__toString());

			$hand = new Hand("Ah", "6d", "6c", "6s", "8c");
			$this->assertEquals("Three of a kind: Sixes", $hand->__toString());
		}

		public function testStraight() {
			$hand = new Hand("Ah", "2h", "3d", "4c", "5c");
			$this->assertEquals("Straight: Ace to five", $hand->__toString());

			$hand = new Hand("Ah", "Kh", "Qd", "Jc", "Tc");
			$this->assertEquals("Straight: Ten to ace", $hand->__toString());

			$hand = new Hand("3h", "4h", "6d", "7c", "5c");
			$this->assertEquals("Straight: Three to seven", $hand->__toString());

			$hand = new Hand("3h", "4h", "6d", "6c", "6s", "7c", "5c");
			$this->assertEquals("Straight: Three to seven", $hand->__toString());

			$hand = new Hand("Ah", "2h", "2d", "3d", "4c", "5c");
			$this->assertEquals("Straight: Ace to five", $hand->__toString());

			$hand = new Hand("Ah", "2h", "7d", "3d", "4c", "5c");
			$this->assertEquals("Straight: Ace to five", $hand->__toString());
		}

		public function testFlush() {
			$hand = new Hand("2h", "6h", "3h", "8h", "Kh");
			$this->assertEquals("Flush: Hearts", $hand->__toString());

			$hand = new Hand("6d", "2d", "3d", "8d", "Kd");
			$this->assertEquals("Flush: Diamonds", $hand->__toString());

			$hand = new Hand("2h", "4h", "6h", "7h", "8h", "9d", "Td");
			$this->assertEquals("Flush: Hearts", $hand->__toString());

			$hand = new Hand("2h", "3h", "4h", "6h", "8h", "4d", "5d");
			$this->assertEquals("Flush: Hearts", $hand->__toString());

			$hand = new Hand("2h", "3h", "4d", "6h", "8h", "4h", "5d");
			$this->assertEquals("Flush: Hearts", $hand->__toString());
		}

		public function testFullHouse() {
			$hand = new Hand("2h", "2d", "2c", "3s", "3d");
			$this->assertEquals("Full house: Twos full of threes", $hand->__toString());

			$hand = new Hand("8h", "Kd", "Kc", "8s", "8d");
			$this->assertEquals("Full house: Eights full of kings", $hand->__toString());

			$hand = new Hand("2h", "2d", "2s", "3h", "3d", "3s");
			$this->assertEquals("Full house: Threes full of twos", $hand->__toString());

			$hand = new Hand("3h", "3d", "3s", "2h", "2d", "2s");
			$this->assertEquals("Full house: Threes full of twos", $hand->__toString());

			$hand = new Hand("3h", "6d", "6c", "3s", "3d");
			$this->assertEquals("Full house: Threes full of sixes", $hand->__toString());

			$hand = new Hand("6h", "3d", "3c", "6s", "6d");
			$this->assertEquals("Full house: Sixes full of threes", $hand->__toString());
		}

		public function testFourOfAKind() {
			$hand = new Hand("3h", "3d", "3c", "3s", "Ah");
			$this->assertEquals("Four of a kind: Threes", $hand->__toString());

			$hand = new Hand("6h", "6d", "6c", "6s", "Ah");
			$this->assertEquals("Four of a kind: Sixes", $hand->__toString());
		}

		public function testStraightFlush() {
			$hand = new Hand("3h", "4h", "5h", "6h", "7h");
			$this->assertEquals("Straight flush: Three to seven", $hand->__toString());

			$hand = new Hand("2d", "3d", "4d", "5d", "6d", "Ad", "Kd");
			$this->assertEquals("Straight flush: Two to six", $hand->__toString());

			$hand = new Hand("2d", "3d", "4d", "5d", "6d", "7d", "8d");
			$this->assertEquals("Straight flush: Four to eight", $hand->__toString());

			$hand = new Hand("2d", "3d", "4d", "5d", "6d", "7h", "8h");
			$this->assertEquals("Straight flush: Two to six", $hand->__toString());
		}

		public function testRoyalFlush() {
			$hand = new Hand("As", "Ks", "Qs", "Js", "Ts");
			$this->assertEquals("Royal flush", $hand->__toString());
		}

		public function testRandom() {
			$hand = new Hand("4s", "Qh", "9c", "As", "6h", "4h", "2h");
			$this->assertEquals("Pair: Fours", $hand->__toString());
		}

		public function testCompareTo() {
			// Between hands
			$this->verifyGreaterThan("2d 2h 3c 4c 8c", "Ah 2c 3h 4h 9d");
			$this->verifyGreaterThan("2c 2s 3h 3d 4s", "2d 2h 3c 4c 8c");
			$this->verifyGreaterThan("2d 2h 2c Td 8h", "5c 5s 3h 3d 4s");
			$this->verifyGreaterThan("3d 4h 5c 6d 7h", "9d 9h 9c Td 8h");
			$this->verifyGreaterThan("Ah 2h 9h Th Jh", "3d 4h 5c 6d 7h");
			$this->verifyGreaterThan("2c 2d 3h 3d 3c", "Ah 2h 9h Th Jh");
			$this->verifyGreaterThan("4h 4c 4d 4s Ah", "2c 2d 3h 3d 3c");
			$this->verifyGreaterThan("4h 5h 6h 7h 8h", "3s 3h 3d 3c 9h");

			// Within hands
			$this->verifyGreaterThan("Ah 2d 3c 8c 9d", "2h 3h 8h 9c Tc");
			$this->verifyGreaterThan("Ah Td 3c 8c 9d", "Ah Td 2c 8c 9d");

			$this->verifyGreaterThan("2d 2h Jc 4c 8c", "2c 2s Tc 4c 8c");
			$this->verifyGreaterThan("2d 2h 4c 5c 6c", "2c 2s 3c 5c 6c");

			$this->verifyGreaterThan("Ah Ad 8c 8s 4d", "Ac As 7c 7s 4c");
			$this->verifyGreaterThan("Ah Ad 8c 8s 4d", "Ac As 8d 8h 3c");
			$this->verifyGreaterThan("Ah Ad 8c 8s 4d", "Kc Ks 8d 8h 4c");

			$this->verifyGreaterThan("4h 4c 4d 5h 6h", "3h 3c 3d 5c 6c");
			$this->verifyGreaterThan("4h 4c 4d 5h 6h", "4h 4c 4d 3c 6c");

			$this->verifyGreaterThan("4h 5h 6c 7c 8h", "3h 4h 5h 6c 7c");
			$this->verifyGreaterThan("Th Jh Qc Kc Ah", "Ah 2h 3h 4c 5c");

			$this->verifyGreaterThan("Ah 2h 3h 8h 9h", "2c 3c 8c 9c Tc");
			$this->verifyGreaterThan("Ah Th 3h 8h 9h", "Ac Tc 2c 8c 9c");
			$this->verifyGreaterThan("2h 4h 5h 7h 9h Th Jd", "2h 4h 5h 7h 9h Td Jd");

			$this->verifyGreaterThan("3h 3d 3c 2h 2c", "2h 2d 2c 3h 3c");
			$this->verifyGreaterThan("8h 8d 8c 2h 2c", "7h 7d 7c 2h 2c");
			$this->verifyGreaterThan("8h 8d 8c 3h 3c", "8h 8d 8c 2h 2c");

			$this->verifyGreaterThan("4c 4h 4d 4s 8h", "3c 3h 3d 3s 8d");
			$this->verifyGreaterThan("4c 4h 4d 4s 8h", "4c 4h 4d 4s 7d");

			$this->verifyGreaterThan("4h 5h 6h 7h 8h", "3c 4c 5c 6c 7c");
			$this->verifyGreaterThan("Ah Kh Qh Jh Th", "Ac 2c 3c 4c 5c");

			// Equal hands
			$this->verifyEqual("Ah 2d 3c 8c 9d", "Ah 2d 3c 8c 9d");
			$this->verifyEqual("Ah 2d 5c 8c 9d Th 4c", "Ah 3d 5c 8c 9d Th 4c");
			$this->verifyEqual("Ah Ad Kd 6c 8c 2h 3c", "Ah Ad Kd 6c 8c 4h 5c");
			$this->verifyEqual("Ah Ad Kd Kc 8c 2h 3c", "Ah Ad Kd Kc 8c 4h 5c");
			$this->verifyEqual("Ah Ad Ac 6c 8c 2h 3c", "Ah Ad Ac 6c 8c 4h 5c");
			$this->verifyEqual("9c Tc Jh Qh Kc 2h 3c", "9c Tc Jh Qh Kc 4h 5c");
			$this->verifyEqual("As 2s 5s 8s 9s Ts 4s", "As 3s 5s 8s 9s Ts 4s");
			$this->verifyEqual("As 2c 5s 8s 9s Ts 4c", "As 3c 5s 8s 9s Ts 4c");
			$this->verifyEqual("Ah Ad As Kh Kd 2h 3c", "Ah Ad As Kh Kd 4h 4c");
			$this->verifyEqual("Ah Ad Ac As 8c 2h 3c", "Ah Ad Ac As 8c 4h 5c");
			$this->verifyEqual("6h 7h 8h 9h Th 2c 3c", "6h 7h 8h 9h Th 4c 5c");

			$this->verifyEqual("Ah Ad Kd 6c 8c 2h 3c", "Ac As Kd 6c 8c 4h 5c");
			$this->verifyEqual("Ah Ad Kd Kc 8c 2h 3c", "Ac As Ks Kh 8c 4h 5c");
			$this->verifyEqual("As 2s 5s 8s 9s 2h 3c", "Ad 2d 5d 8d 9d 4h 5c");
			$this->verifyEqual("6h 7h 8h 9h Th 2c 3c", "6d 7d 8d 9d Td 4c 5c");
		}

		private function verifyGreaterThan($h1, $h2) {
			$h1c = explode(" ", $h1);
			$h2c = explode(" ", $h2);

			$ref = new ReflectionClass("Hand");
			$hand1 = $ref->newInstanceArgs($h1c);
			$hand2 = $ref->newInstanceArgs($h2c);

			$this->assertTrue($hand1->compareTo($hand2) > 0, "$hand1 > $hand2");
			$this->assertTrue($hand2->compareTo($hand1) < 0, "$hand2 < $hand1");
		}

		private function verifyEqual($h1, $h2) {
			$h1c = explode(" ", $h1);
			$h2c = explode(" ", $h2);

			$ref = new ReflectionClass("Hand");
			$hand1 = $ref->newInstanceArgs($h1c);
			$hand2 = $ref->newInstanceArgs($h2c);

			$this->assertEquals(0, $hand1->compareTo($hand2), "$hand1 == $hand2");
			$this->assertEquals(0, $hand2->compareTo($hand1), "$hand2 == $hand1");
		}

	}
?>
