<?
	require_once 'PHPUnit/Framework.php';

	require_once("../code/autoloader.php");

	class PlayerTest extends PHPUnit_Framework_TestCase {
		public function testConstructor() {
			$player = new Player("asdf");
			$this->assertEquals("Player", get_class($player));
			$this->assertEquals("asdf", $player->getName());
		}

		public function testAddCard() {
			$p = new Player("asdf");
			$p->addCard(new Card("Ah"));

			$cards = $p->getCards();
			$this->assertEquals($cards[0]->__toString(), "Ah");

			$p->addCard(new Card("Jd"));
			$cards = $p->getCards();
			$this->assertEquals($cards[0]->__toString(), "Ah");
			$this->assertEquals($cards[1]->__toString(), "Jd");

			$p->clearCards();
			$this->assertEquals(0, count($p->getCards()));
		}

		public function testAddMoney() {

			$p = new Player("asdf");
			$this->assertEquals(0, $p->getMoney());

			$p->addMoney(500);
			$this->assertEquals(500, $p->getMoney());

			$p->addMoney(-250);
			$this->assertEquals(250, $p->getMoney());

			$p->addMoney(-250);
			$this->assertEquals(0, $p->getMoney());

			$e = false;
			try {
				$p->addMoney(-1);
			} catch (Exception $e) {
				$e = true;
			}
			$this->assertEquals(true, $e);
		}

		public function testEquals() {
			$p = new Player("asdf");
			$p2 = new Player("asdf");
			$p3 = new Player("jkl");

			$this->assertTrue($p->equals($p2));
			$this->assertTrue($p2->equals($p));
			$this->assertFalse($p->equals($p3));
			$this->assertFalse($p3->equals($p));
			$this->assertFalse($p2->equals($p3));
			$this->assertFalse($p3->equals($p2));
		}

		public function testGetCardStr() {
			$p = new Player("asdf");
			$p->addCard(new Card("Ah"));
			$p->addCard(new Card("Ad"));

			$this->assertEquals("Ah Ad", $p->getCardStr());
		}

		public function testFolded() {
			$p = new Player("asdf");
			$this->assertEquals(false, $p->folded());
			$this->assertEquals(false, $p->folded());
			$p->folded(true);
			$this->assertEquals(true, $p->folded());
			$this->assertEquals(true, $p->folded());
			$p->folded(false);
			$this->assertEquals(false, $p->folded());
			$this->assertEquals(false, $p->folded());
		}

		public function testAllIn() {
			$p = new Player("asdf");
			$this->assertEquals(false, $p->allIn());
			$this->assertEquals(false, $p->allIn());
			$p->allIn(true);
			$this->assertEquals(true, $p->allIn());
			$this->assertEquals(true, $p->allIn());
			$p->allIn(false);
			$this->assertEquals(false, $p->allIn());
			$this->assertEquals(false, $p->allIn());
		}

		public function testEliminated() {
			$p = new Player("asdf");
			$this->assertEquals(false, $p->eliminated());
			$this->assertEquals(false, $p->eliminated());
			$p->eliminated(true);
			$this->assertEquals(true, $p->eliminated());
			$this->assertEquals(true, $p->eliminated());
			$p->eliminated(false);
			$this->assertEquals(false, $p->eliminated());
			$this->assertEquals(false, $p->eliminated());
		}

		public function testActive() {
			$p = new Player("asdf");
			$this->assertEquals(true, $p->active());
			$this->assertEquals(true, $p->active());
			$p->allIn(true);
			$this->assertEquals(false, $p->active());
			$this->assertEquals(false, $p->active());
			$p->allIn(false);
			$this->assertEquals(true, $p->active());
			$this->assertEquals(true, $p->active());
			$p->folded(true);
			$this->assertEquals(false, $p->active());
			$this->assertEquals(false, $p->active());
			$p->allIn(true);
			$this->assertEquals(false, $p->active());
			$this->assertEquals(false, $p->active());
		}

		public function testToStr() {
			$p = new Player("asdf");
			$this->assertEquals("asdf", $p->__toString());
		}
	}
?>
