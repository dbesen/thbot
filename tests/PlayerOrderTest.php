<?
	require_once 'PHPUnit/Framework.php';

	require_once("../code/autoloader.php");

	class PlayerOrderTest extends PHPUnit_Framework_TestCase {

		public function testOnePlayer() {
			$p1 = new Player("Player 1");
			$p = new PlayerOrder(array($p1));
			$this->assertEquals("Player 1", $p->getCurrentPlayer()->getName());
			$p->acted($p1);
			$this->assertEquals("Player 1", $p->getCurrentPlayer()->getName());
		}

		public function testTwoPlayers() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");

			$p = new PlayerOrder(array($p1, $p2));

			$this->assertTrue($p->getDealer()->equals($p1));
			$this->assertTrue($p->getSB()->equals($p1));
			$this->assertTrue($p->getBB()->equals($p2));

			$this->assertEquals("Player 1", $p->getCurrentPlayer()->getName());
			$p->acted($p1);
			$this->assertEquals("Player 2", $p->getCurrentPlayer()->getName());
		}

		public function testDealerSBBB() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p = new PlayerOrder(array($p1, $p2, $p3));
			$this->assertTrue($p->getDealer()->equals($p1));
			$this->assertTrue($p->getSB()->equals($p2));
			$this->assertTrue($p->getBB()->equals($p3));
			$p->finishRound();
			$this->assertTrue($p->getDealer()->equals($p2));
			$this->assertTrue($p->getSB()->equals($p3));
			$this->assertTrue($p->getBB()->equals($p1));
		}

		public function testCurrentPlayer() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p = new PlayerOrder(array($p1, $p2, $p3));
			$this->assertTrue($p->getCurrentPlayer()->equals($p2));
			$this->assertTrue($p->isCurrentPlayer($p2));
			$p->acted($p2);
			$this->assertTrue($p->getCurrentPlayer()->equals($p3));
			$this->assertTrue($p->isCurrentPlayer($p3));
			$p->acted($p3);
			$this->assertTrue($p->getCurrentPlayer()->equals($p1));
			$p->acted($p1);
			$this->assertTrue($p->getCurrentPlayer()->equals($p2));
			$p->acted($p2);
			$this->assertTrue($p->getCurrentPlayer()->equals($p3));
			$p->acted($p3);
			$this->assertTrue($p->getCurrentPlayer()->equals($p1));

			$p->finishRound();
			$this->assertTrue($p->getCurrentPlayer()->equals($p3));
			$p->acted($p3);
			$this->assertTrue($p->getCurrentPlayer()->equals($p1));
			$p->acted($p1);
			$this->assertTrue($p->getCurrentPlayer()->equals($p2));
			$p->acted($p2);
			$p3->folded(true);
			$this->assertTrue($p->getCurrentPlayer()->equals($p1));
		}

		public function testSBActsFirst3Players() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p = new PlayerOrder(array($p1, $p2, $p3));

			$this->assertEquals("Player 1", $p->getDealer()->getName());
			$this->assertEquals("Player 2", $p->getSB()->getName());
			$this->assertEquals("Player 3", $p->getBB()->getName());

			$this->assertEquals("Player 2", $p->getCurrentPlayer()->getName());
			$p->acted($p2);
			$this->assertEquals("Player 3", $p->getCurrentPlayer()->getName());
			$p->acted($p3);
			$this->assertEquals("Player 1", $p->getCurrentPlayer()->getName());
			$p->acted($p1);
			$this->assertEquals("Player 2", $p->getCurrentPlayer()->getName());
		}

		public function testSBActsFirst2Players() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p = new PlayerOrder(array($p1, $p2));

			$this->assertEquals("Player 1", $p->getDealer()->getName());
			$this->assertEquals("Player 1", $p->getSB()->getName());
			$this->assertEquals("Player 2", $p->getBB()->getName());

			$this->assertEquals("Player 1", $p->getCurrentPlayer()->getName());
			$p->acted($p1);
			$this->assertEquals("Player 2", $p->getCurrentPlayer()->getName());
			$p->acted($p2);
			$this->assertEquals("Player 1", $p->getCurrentPlayer()->getName());
			$p->acted($p1);
			$this->assertEquals("Player 2", $p->getCurrentPlayer()->getName());
			$p->acted($p2);
			$this->assertEquals("Player 1", $p->getCurrentPlayer()->getName());
		}

		public function testSBActsFirst4Players() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p4 = new Player("Player 4");
			$p = new PlayerOrder(array($p1, $p2, $p3, $p4));

			$this->assertEquals("Player 1", $p->getDealer()->getName());
			$this->assertEquals("Player 2", $p->getSB()->getName());
			$this->assertEquals("Player 3", $p->getBB()->getName());

			$this->assertEquals("Player 2", $p->getCurrentPlayer()->getName());
			$p->acted($p2);
			$this->assertEquals("Player 3", $p->getCurrentPlayer()->getName());
			$p->acted($p3);
			$this->assertEquals("Player 4", $p->getCurrentPlayer()->getName());
			$p->acted($p4);
			$this->assertEquals("Player 1", $p->getCurrentPlayer()->getName());
			$p->acted($p1);
			$this->assertEquals("Player 2", $p->getCurrentPlayer()->getName());
			$p->acted($p2);
			$this->assertEquals("Player 3", $p->getCurrentPlayer()->getName());
			$p->acted($p3);
			$this->assertEquals("Player 4", $p->getCurrentPlayer()->getName());
			$p->acted($p4);
			$this->assertEquals("Player 1", $p->getCurrentPlayer()->getName());
			$p->acted($p1);
			$this->assertEquals("Player 2", $p->getCurrentPlayer()->getName());
		}

		public function testFolding() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p4 = new Player("Player 4");
			$p = new PlayerOrder(array($p1, $p2, $p3, $p4));

			$this->assertLineup($p, $p1, $p2, $p3);

			$p->acted($p2); // small blind
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p3); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p4);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p1);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p2->folded(true);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p3);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p4);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p1);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p3);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p4);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p1);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p3->folded(true);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p4);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p1);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p4);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p1);
			$this->assertLineup($p, $p1, $p2, $p3);
		}

		public function testDeadNormalPlayer() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p4 = new Player("Player 4");
			$p = new PlayerOrder(array($p1, $p2, $p3, $p4));

			$this->assertLineup($p, $p1, $p2, $p3);

			$p->acted($p2); // small blind
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p3); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p4);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p1);
			$this->assertLineup($p, $p1, $p2, $p3);

			$p->removePlayer($p1);
			$p->finishRound();

			$this->assertLineup($p, $p2, $p3, $p4);
			$p->acted($p3); // small blind
			$this->assertLineup($p, $p2, $p3, $p4);
			$p->acted($p4); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p2, $p3, $p4);
			$p->acted($p2);
			$this->assertLineup($p, $p2, $p3, $p4);
			$p->acted($p3);
			$this->assertLineup($p, $p2, $p3, $p4);
			$p->acted($p4);
			$this->assertLineup($p, $p2, $p3, $p4);
			$p->acted($p2);
			$this->assertLineup($p, $p2, $p3, $p4);

			$p->removePlayer($p2);
			$p->finishRound();

			$this->assertLineup($p, $p3, $p3, $p4);
			$p->acted($p3); // small blind
			$this->assertLineup($p, $p3, $p3, $p4);
			$p->acted($p4); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p3, $p3, $p4);
			$p->acted($p3);
			$this->assertLineup($p, $p3, $p3, $p4);
			$p->acted($p4);
			$this->assertLineup($p, $p3, $p3, $p4);
		}

		public function testDeadDealer() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p4 = new Player("Player 4");
			$p = new PlayerOrder(array($p1, $p2, $p3, $p4));

			$this->assertLineup($p, $p1, $p2, $p3);

			$p->acted($p2); // small blind
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p3); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p4);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p1);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p2);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p3);
			$this->assertLineup($p, $p1, $p2, $p3);

			$p->removePlayer($p2); // p2 *will be* the dealer next round
			$p->finishRound();
			$this->assertLineup($p, null, $p3, $p4);
			$p->acted($p3); // small blind
			$this->assertLineup($p, null, $p3, $p4);
			$p->acted($p4); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, null, $p3, $p4);
			$p->acted($p1);
			$this->assertLineup($p, null, $p3, $p4);

			// Make sure it goes back to normal
			$p->finishRound();
			$this->assertLineup($p, $p3, $p4, $p1);
			$p->acted($p4); // small blind
			$this->assertLineup($p, $p3, $p4, $p1);
			$p->acted($p1); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p3, $p4, $p1);
			$p->acted($p3);
			$this->assertLineup($p, $p3, $p4, $p1);

			$p->removePlayer($p4); // p4 will be dealer
			$p->finishRound();

			// Normally we would swap small and big blind when going to heads-up, but we don't since there's a dead dealer.
			$this->assertLineup($p, null, $p1, $p3);
			$p->acted($p1); // small blind
			$this->assertLineup($p, null, $p1, $p3);
			$p->acted($p3); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, null, $p1, $p3);
			$p->acted($p1);
			$this->assertLineup($p, null, $p1, $p3);
			$p->acted($p3);
			$this->assertLineup($p, null, $p1, $p3);

			// *now* they switch places, since the round with the dead dealer ended.
			// But the dealer advanced! so it's actually the same lineup as the last round
			$p->finishRound();

			$this->assertLineup($p, $p1, $p1, $p3);
			$p->acted($p1); // small blind
			$this->assertLineup($p, $p1, $p1, $p3);
			$p->acted($p3); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p1, $p1, $p3);
			$p->acted($p1);
			$this->assertLineup($p, $p1, $p1, $p3);
			$p->acted($p3);
			$this->assertLineup($p, $p1, $p1, $p3);

			// Now they swap places normally.
			$p->finishRound();

			$this->assertLineup($p, $p3, $p3, $p1);
			$p->acted($p3); // small blind
			$this->assertLineup($p, $p3, $p3, $p1);
			$p->acted($p1); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p3, $p3, $p1);
			$p->acted($p3);
			$this->assertLineup($p, $p3, $p3, $p1);
			$p->acted($p1);
			$this->assertLineup($p, $p3, $p3, $p1);
		}

		private function assertLineup(PlayerOrder $p, $dealer, $sb, $bb) {
			if($dealer == null) $this->assertEquals(null, $p->getDealer());
			else $this->assertTrue($dealer->equals($p->getDealer()), "expected $dealer to be the dealer, got {$p->getDealer()}");
			if($sb == null) $this->assertEquals(null, $p->getSB());
			else $this->assertTrue($sb->equals($p->getSB()), "expected $sb to be the small blind, got {$p->getSB()}");
			$this->assertTrue($bb->equals($p->getBB()));
		}

		public function testDeadSB() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p4 = new Player("Player 4");
			$p = new PlayerOrder(array($p1, $p2, $p3, $p4));

			$this->assertLineup($p, $p1, $p2, $p3);

			$p->acted($p2); // small blind
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p3); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p4);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p1);
			$this->assertLineup($p, $p1, $p2, $p3);

			$p->removePlayer($p3); // p3 *will be* the small blind next round
			$p->finishRound();

			$this->assertLineup($p, $p2, null, $p4);
			$p->acted($p4); // posts big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p2, null, $p4);
			$p->acted($p1);
			$this->assertLineup($p, $p2, null, $p4);
			$p->acted($p2);
			$this->assertLineup($p, $p2, null, $p4);

			$p->finishRound();

			// Now we have a dead dealer, since we had a dead small.
			$this->assertLineup($p, null, $p4, $p1);
			$p->acted($p4); // small blind
			$this->assertLineup($p, null, $p4, $p1);
			$p->acted($p1); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, null, $p4, $p1);
			$p->acted($p2);
			$this->assertLineup($p, null, $p4, $p1);

			$p->finishRound();

			// Now we're back to normal.
			$this->assertLineup($p, $p4, $p1, $p2);
			$p->acted($p1); // small blind
			$this->assertLineup($p, $p4, $p1, $p2);
			$p->acted($p2); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p4, $p1, $p2);
			$p->acted($p4);
			$this->assertLineup($p, $p4, $p1, $p2);

			$p->removePlayer($p2); // p2 will be small blind
			$p->finishRound();

			// They don't swap places since there's a dead small and it's not really heads-up yet.
			$this->assertLineup($p, $p1, null, $p4);
			$p->acted($p4); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p1, null, $p4);
			$p->acted($p1);
			$this->assertLineup($p, $p1, null, $p4);

			$p->finishRound();

			$this->assertLineup($p, null, $p4, $p1); // And now there's a dead dealer.
			$p->acted($p4); // small blind
			$this->assertLineup($p, null, $p4, $p1);
			$p->acted($p1); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, null, $p4, $p1);
			$p->acted($p4);
			$this->assertLineup($p, null, $p4, $p1);
			$p->acted($p1);
			$this->assertLineup($p, null, $p4, $p1);

			$p->finishRound();

			// No more dead dealer, so we're heads up, so we switch places and advance, so p4 is the small blind again.
			$this->assertLineup($p, $p4, $p4, $p1);
			$p->acted($p4); // small blind
			$this->assertLineup($p, $p4, $p4, $p1);
			$p->acted($p1); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p4, $p4, $p1);
			$p->acted($p4);
			$this->assertLineup($p, $p4, $p4, $p1);
			$p->acted($p1);
			$this->assertLineup($p, $p4, $p4, $p1);

			$p->finishRound();

			// Back to normal.
			$this->assertLineup($p, $p1, $p1, $p4);
			$p->acted($p1); // small blind
			$this->assertLineup($p, $p1, $p1, $p4);
			$p->acted($p4); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p1, $p1, $p4);
			$p->acted($p1);
			$this->assertLineup($p, $p1, $p1, $p4);
			$p->acted($p4);
			$this->assertLineup($p, $p1, $p1, $p4);
		}

		public function testDeadBB() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p4 = new Player("Player 4");
			$p = new PlayerOrder(array($p1, $p2, $p3, $p4));

			$this->assertLineup($p, $p1, $p2, $p3);

			$p->acted($p2); // small blind
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p3); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p4);
			$this->assertLineup($p, $p1, $p2, $p3);
			$p->acted($p1);
			$this->assertLineup($p, $p1, $p2, $p3);

			$p->removePlayer($p4); // p4 *will be* the big blind next round
			$p->finishRound();

			$this->assertLineup($p, $p2, $p3, $p1); // No such thing as a dead big.  Screwy poker.
			$p->acted($p3); // small blind
			$this->assertLineup($p, $p2, $p3, $p1);
			$p->acted($p1); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p2, $p3, $p1);
			$p->acted($p2);
			$this->assertLineup($p, $p2, $p3, $p1);
			$p->acted($p3);
			$this->assertLineup($p, $p2, $p3, $p1);
			$p->acted($p1);
			$this->assertLineup($p, $p2, $p3, $p1);
			$p->acted($p2);
			$this->assertLineup($p, $p2, $p3, $p1);

			$p->removePlayer($p2);
			$p->finishRound();

			// $p3 would naturally be the dealer, and we swap places for heads-up, so he's small blind also.
			$this->assertLineup($p, $p3, $p3, $p1);
			$p->acted($p3); // small blind
			$this->assertLineup($p, $p3, $p3, $p1);
			$p->acted($p1); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p3, $p3, $p1);
			$p->acted($p3);
			$this->assertLineup($p, $p3, $p3, $p1);
			$p->acted($p1);
			$this->assertLineup($p, $p3, $p3, $p1);
		}

		public function testActedSinceRaise() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p4 = new Player("Player 4");
			$p = new PlayerOrder(array($p1, $p2, $p3, $p4));

			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p2); // small blind
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p3); // big blind
			$p->postedBlinds();
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p4);
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p1);
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p2);
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p3);
			$this->assertTrue($p->allPlayersActedSinceRaise());


			$p = new PlayerOrder(array($p1, $p2, $p3, $p4));
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p2); // small blind
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p3); // big blind
			$p->postedBlinds();
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p4, true);
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p1);
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p2);
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p3);
			$this->assertTrue($p->allPlayersActedSinceRaise());

			$p = new PlayerOrder(array($p1, $p2, $p3, $p4));
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p2); // small blind
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p3); // big blind
			$p->postedBlinds();
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p4);
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p1, true);
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p2);
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p3);
			$this->assertFalse($p->allPlayersActedSinceRaise());
			$p->acted($p4);
			$this->assertTrue($p->allPlayersActedSinceRaise());
		}

		public function testOrder2Players() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p = new PlayerOrder(array($p1, $p2));

			$this->assertLineup($p, $p1, $p1, $p2);
			$this->assertEquals("Player 1", $p->getCurrentPlayer()->getName());
			$p->acted($p1); // small blind
			$this->assertLineup($p, $p1, $p1, $p2);
			$this->assertEquals("Player 2", $p->getCurrentPlayer()->getName());
			$p->acted($p2); // big blind
			$p->postedBlinds();
			$this->assertLineup($p, $p1, $p1, $p2);
			$this->assertEquals("Player 1", $p->getCurrentPlayer()->getName());
			$p->acted($p1);
			$this->assertLineup($p, $p1, $p1, $p2);
			$this->assertEquals("Player 2", $p->getCurrentPlayer()->getName());
			$p->acted($p2);
			$p->finishBettingRound();
			// Now, they swap positions (poker is really just weird)
			$this->assertLineup($p, $p1, $p1, $p2);
			$this->assertEquals("Player 2", $p->getCurrentPlayer()->getName());
			$p->acted($p2);
			$this->assertLineup($p, $p1, $p1, $p2);
			$this->assertEquals("Player 1", $p->getCurrentPlayer()->getName());
			$p->acted($p1);
			$p->finishBettingRound();
			$this->assertLineup($p, $p1, $p1, $p2);
			$this->assertEquals("Player 2", $p->getCurrentPlayer()->getName());
			$p->acted($p2);
			$this->assertLineup($p, $p1, $p1, $p2);
			$this->assertEquals("Player 1", $p->getCurrentPlayer()->getName());
			$p->acted($p1);
		}
	}
?>

