<?
	require_once 'PHPUnit/Framework.php';

	require_once("../code/autoloader.php");

	class CardTest extends PHPUnit_Framework_TestCase {

		public function testConstructor() {
			$card = new Card("Ah");
			$this->assertEquals("Card", get_class($card), "Ah");

			$card = new Card("ah");
			$this->assertEquals("Card", get_class($card), "ah");

			$e = false;
			try {
				$card = new Card("ahdf");
			} catch (Exception $e) {
				$e = true;
			}
			$this->assertEquals(true, $e, "ahdf");

			$e = false;
			try {
				$card = new Card("ar");
			} catch (Exception $e) {
				$e = true;
			}
			$this->assertEquals(true, $e, "ar");
		}

		public function testToString() {
			$card = new Card("Ah");
			$this->assertEquals("Ah", $card->__toString());

			$card = new Card("Kd");
			$this->assertEquals("Kd", $card->__toString());

			$card = new Card("3c");
			$this->assertEquals("3c", $card->__toString());
		}

		public function testCompareTo() {
			$this->bigger("Ah", "2h");
			$this->bigger("Ad", "2d");
			$this->bigger("Ah", "Kh");
			$this->bigger("Ah", "Qh");
			$this->bigger("Ah", "Jh");
			$this->bigger("Ah", "Th");
			$this->bigger("Ah", "9h");
			$this->bigger("Ah", "2h");
			$this->bigger("Kh", "Qh");
			$this->bigger("Qh", "Jh");
			$this->bigger("Jh", "Th");
			$this->bigger("Th", "9h");
			$this->bigger("9h", "8h");
			$this->bigger("9h", "2h");
			$this->bigger("3h", "2h");

			$card1 = new Card("Ah");
			$card2 = new Card("Ad");
			$this->assertTrue($card1->compareTo($card2) == 0);
			$this->assertTrue($card2->compareTo($card1) == 0);
		}

		private function bigger($c1, $c2) {
			$card1 = new Card($c1);
			$card2 = new Card($c2);
			$this->assertTrue($card1->compareTo($card2) > 0);
			$this->assertTrue($card2->compareTo($card1) < 0);
		}

		public function testRankStr() {
			$c = new Card("Ah"); $this->assertEquals("Ace", $c->rankStr());
			$c = new Card("Ad"); $this->assertEquals("Ace", $c->rankStr());
			$c = new Card("2h"); $this->assertEquals("Two", $c->rankStr());
			$c = new Card("3h"); $this->assertEquals("Three", $c->rankStr());
			$c = new Card("4h"); $this->assertEquals("Four", $c->rankStr());
			$c = new Card("5h"); $this->assertEquals("Five", $c->rankStr());
			$c = new Card("6h"); $this->assertEquals("Six", $c->rankStr());
			$c = new Card("7h"); $this->assertEquals("Seven", $c->rankStr());
			$c = new Card("8h"); $this->assertEquals("Eight", $c->rankStr());
			$c = new Card("9h"); $this->assertEquals("Nine", $c->rankStr());
			$c = new Card("Th"); $this->assertEquals("Ten", $c->rankStr());
			$c = new Card("Jh"); $this->assertEquals("Jack", $c->rankStr());
			$c = new Card("Qh"); $this->assertEquals("Queen", $c->rankStr());
			$c = new Card("Kh"); $this->assertEquals("King", $c->rankStr());
		}

		public function testSuitStr() {
			$c = new Card("Ah"); $this->assertEquals("Hearts", $c->suitStr());
			$c = new Card("2h"); $this->assertEquals("Hearts", $c->suitStr());
			$c = new Card("Ad"); $this->assertEquals("Diamonds", $c->suitStr());
			$c = new Card("Ac"); $this->assertEquals("Clubs", $c->suitStr());
			$c = new Card("As"); $this->assertEquals("Spades", $c->suitStr());
		}

		public function testGetIrcStr() {
			$c = new Card("Ah"); $this->assertEquals("01,05Ah", $c->getIrcStr());
			$c = new Card("Ac"); $this->assertEquals("01,03Ac", $c->getIrcStr());
			$c = new Card("Ad"); $this->assertEquals("00,02Ad", $c->getIrcStr());
			$c = new Card("As"); $this->assertEquals("01,15As", $c->getIrcStr());
			$c = new Card("3h"); $this->assertEquals("01,053h", $c->getIrcStr());
		}
	}
?>
