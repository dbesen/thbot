<?
/*
randomizePlayerOrder() // would be automatic, but have to test
getCurrentPotSizes() // returns array... and who's in which pot somehow?
allIn(player)
*/
	require_once 'PHPUnit/Framework.php';

	require_once("../code/autoloader.php");

	class GameTest extends PHPUnit_Framework_TestCase {

		// To test:
		// Getting all info needed to display the game

		function testPlayGame() {
			$g = new Game();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->addPlayer($p3);
			$g->setInitialStackSize(500);
			$g->setBlinds(5, 10);

			// Notice no randomizePlayerOrder call

			$g->beginGame();

			$e = false;
			try {
				$g->addPlayer(new Player("Player 4"));
			} catch(Exception $e) {
				$e = true;
			}
			$this->assertEquals(true, $e);

			$this->assertTrue($g->getCurrentDealer()->equals($p1), "Dealer is p1");
			$this->assertTrue($g->getCurrentSB()->equals($p2), "SB is p2");
			$this->assertTrue($g->getCurrentBB()->equals($p3), "BB is p3");

			$this->assertEquals(500, $p1->getMoney());
			$this->assertEquals(500, $p2->getMoney());
			$this->assertEquals(500, $p3->getMoney());

			$this->assertEquals(5, strlen($p1->getCardStr()));
			$this->assertEquals(5, strlen($p2->getCardStr()));
			$this->assertEquals(5, strlen($p3->getCardStr()));
			$this->assertEquals("", $g->getBoardStr());

			$g->postBlinds();

			$this->assertEquals(500, $p1->getMoney());
			$this->assertEquals(495, $p2->getMoney());
			$this->assertEquals(490, $p3->getMoney());

			$this->assertEquals("It's not your turn", $g->bet($p2, 100));
			$this->assertEquals("It's not your turn", $g->bet($p3, 100));

			$this->assertEquals(null, $g->bet($p1, 100));

			// Now, p2 can bet exactly 100, or anything >= 200
			$this->assertEquals("That's not enough to call", $g->bet($p2, 10));
			$this->assertEquals('The minimum raise is $90', $g->raise($p2, 50));
			$this->assertEquals('The minimum raise is $90', $g->raise($p2, 85));
			$this->assertEquals(null, $g->call($p2));

			$this->assertEquals("It's not your turn", $g->bet($p1, 100));

			$this->assertEquals("That's not enough to call", $g->bet($p3, 10));
			$this->assertEquals('The minimum raise is $90', $g->raise($p3, 50));
			$this->assertEquals('The minimum raise is $90', $g->raise($p3, 85));
			$this->assertEquals('The minimum raise is $90', $g->bet($p3, 175));
			$this->assertEquals("", $g->getBoardStr());
			$this->assertEquals(null, $g->raise($p3, 100));

			$this->assertEquals(400, $p1->getMoney());
			$this->assertEquals(400, $p2->getMoney());
			$this->assertEquals(300, $p3->getMoney());

			$this->assertEquals("", $g->getBoardStr());

			$this->assertEquals("It's not your turn", $g->fold($p3));
			$this->assertEquals("It's not your turn", $g->fold($p2));
			$this->assertEquals(null, $g->fold($p1));

			$this->assertEquals("", $g->getBoardStr());

			$this->assertEquals("That's not enough to call", $g->check($p2));
			$this->assertEquals(null, $g->call($p2));

			// -- flop
			$this->assertEquals(8, strlen($g->getBoardStr()));

			$this->assertEquals(null, $g->check($p2));
			$this->assertEquals(null, $g->raise($p3, 100));
			$this->assertEquals(null, $g->fold($p2));

			$this->assertEquals(400, $p1->getMoney());
			$this->assertEquals(300, $p2->getMoney());
			$this->assertEquals(800, $p3->getMoney());

			$winners = $g->getWinners();
			$this->assertEquals(1, count($winners));
			$this->assertEquals(600, $winners['Player 3']);

			$muckers = $g->getMuckers();
			$this->assertEquals(0, count($muckers));

			$this->assertEquals(-1, $g->getBettingRound());

			// Next round
			$g->nextRound();

			$this->assertTrue($g->getCurrentDealer()->equals($p2), "Dealer is p2");
			$this->assertTrue($g->getCurrentSB()->equals($p3), "SB is p3");
			$this->assertTrue($g->getCurrentBB()->equals($p1), "BB is p1");
			$this->assertEquals(0, strlen($g->getBoardStr()));

			$g->postBlinds();

			$this->assertEquals(null, $g->fold($p2));
			$this->assertEquals(null, $g->call($p3));
			$this->assertEquals(0, strlen($g->getBoardStr()));
			$this->assertEquals(null, $g->check($p1));

			// -- flop, p1 and p3 remain, p3 acts next

			$this->assertEquals(8, strlen($g->getBoardStr()));
			$this->assertEquals(null, $g->check($p3));
			$this->assertEquals(null, $g->check($p1));
			$this->assertEquals(11, strlen($g->getBoardStr()));

			$this->assertEquals(null, $g->check($p3));
			$this->assertEquals(null, $g->check($p1));
			$this->assertEquals(14, strlen($g->getBoardStr()));
			$this->assertEquals(null, $g->check($p3));
			$this->assertEquals(14, strlen($g->getBoardStr()));
			$this->assertEquals(null, $g->check($p1));

			// Round over
			$this->assertEquals(4, $g->getBettingRound());
			$g->nextRound();

			$this->assertEquals(0, strlen($g->getBoardStr()));
			$this->assertTrue($g->getCurrentDealer()->equals($p3), "Dealer is p3");
		}

		public function testBetSizes() {
			$g = new Game();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->addPlayer($p3);
			$g->setInitialStackSize(100);
			$g->setBlinds(5, 10);

			$g->beginGame();
			$g->postBlinds();

			$this->assertEquals("Bets must be a multiple of the small blind", $g->bet($p1, 21));
			$this->assertEquals("Bets must be a multiple of the small blind", $g->bet($p1, 24));
			$this->assertEquals("Bets must be a multiple of the small blind", $g->bet($p1, 20.5));
		}

		public function testMinBlinding() {
			$g = new Game();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p4 = new Player("Player 4");
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->addPlayer($p3);
			$g->addPlayer($p4);
			$g->setInitialStackSize(5);
			$g->setBlinds(5, 10);

			$g->beginGame();

			$p1->addMoney(95);
			$p4->addMoney(95);

			// p1 is dealer, p2 is small blind, p3 is big blind

			$this->assertEquals($p1->getMoney(), 100);
			$this->assertEquals($p2->getMoney(), 5);
			$this->assertEquals($p3->getMoney(), 5);
			$this->assertEquals($p4->getMoney(), 100);

			$g->postBlinds();

			$this->assertEquals($p1->getMoney(), 100);
			$this->assertEquals($p2->getMoney(), 0);
			$this->assertEquals($p3->getMoney(), 0);
			$this->assertEquals($p4->getMoney(), 100);

			$g->call($p4);

			$g->call($p1);

			// -- flop

			$this->assertEquals(8, strlen($g->getBoardStr()));

			$this->assertEquals($p1->getMoney(), 90);
			$this->assertEquals($p4->getMoney(), 90);

			// Now, if p1 wins, he wins 30
			// If p2 wins, he wins 20 -- the last 10 is between p1 and p4
			// If p3 wins, he wins 20
			// If p4 wins, he wins 30

			// The min raise should be 10

			$this->assertEquals("The minimum raise is $10", $g->raise($p4, 5));
		}

		public function testAllIn() {
			$g = new Game();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p4 = new Player("Player 4");
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->addPlayer($p3);
			$g->addPlayer($p4);
			$g->setInitialStackSize(100);
			$g->setBlinds(5, 10);

			$g->beginGame();

			$g->postBlinds();

			$this->assertEquals(100, $p1->getMoney());
			$this->assertEquals(95, $p2->getMoney());
			$this->assertEquals(90, $p3->getMoney());
			$this->assertEquals(100, $p4->getMoney());

			$this->assertEquals(null, $g->call($p4));

			$this->assertEquals(100, $p1->getMoney());
			$this->assertEquals(95, $p2->getMoney());
			$this->assertEquals(90, $p3->getMoney());
			$this->assertEquals(90, $p4->getMoney());

			$this->assertEquals(null, $g->allIn($p1));

			$this->assertEquals(0, $p1->getMoney());
			$this->assertEquals(95, $p2->getMoney());
			$this->assertEquals(90, $p3->getMoney());
			$this->assertEquals(90, $p4->getMoney());

			$this->assertEquals(null, $g->call($p2));

			$this->assertEquals(0, $p1->getMoney());
			$this->assertEquals(0, $p2->getMoney());
			$this->assertEquals(90, $p3->getMoney());
			$this->assertEquals(90, $p4->getMoney());

			$this->assertEquals(null, $g->fold($p3));

			$this->assertEquals(0, $p1->getMoney());
			$this->assertEquals(0, $p2->getMoney());
			$this->assertEquals(90, $p3->getMoney());
			$this->assertEquals(90, $p4->getMoney());

			$this->assertEquals(0, strlen($g->getBoardStr()));

			// -- showdown

			$this->assertEquals(null, $g->fold($p4));

			$this->assertEquals(-2, $g->getBettingRound());
			$g->nextRound();
			$this->assertEquals(0, strlen($g->getBoardStr()));
		}

		public function testAllIn2() {
			$g = new Game();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p4 = new Player("Player 4");
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->addPlayer($p3);
			$g->addPlayer($p4);
			$g->setInitialStackSize(100);
			$g->setBlinds(5, 10);

			$p2->addMoney(100);

			$g->beginGame();

			$g->postBlinds();

			$this->assertEquals(100, $p1->getMoney());
			$this->assertEquals(195, $p2->getMoney());
			$this->assertEquals(90, $p3->getMoney());
			$this->assertEquals(100, $p4->getMoney());

			$this->assertEquals(null, $g->call($p4));

			$this->assertEquals(100, $p1->getMoney());
			$this->assertEquals(195, $p2->getMoney());
			$this->assertEquals(90, $p3->getMoney());
			$this->assertEquals(90, $p4->getMoney());

			$this->assertEquals(null, $g->allIn($p1));

			$this->assertEquals(0, $p1->getMoney());
			$this->assertEquals(195, $p2->getMoney());
			$this->assertEquals(90, $p3->getMoney());
			$this->assertEquals(90, $p4->getMoney());

			$this->assertEquals(null, $g->call($p2));

			$this->assertEquals(0, $p1->getMoney());
			$this->assertEquals(100, $p2->getMoney());
			$this->assertEquals(90, $p3->getMoney());
			$this->assertEquals(90, $p4->getMoney());

			$this->assertEquals(null, $g->fold($p3));

			$this->assertEquals(0, strlen($g->getBoardStr()));

			$this->assertEquals(0, $p1->getMoney());
			$this->assertEquals(100, $p2->getMoney());
			$this->assertEquals(90, $p3->getMoney());
			$this->assertEquals(90, $p4->getMoney());

			// -- showdown
			$this->assertEquals(null, $g->fold($p4));

			$this->assertEquals(-2, $g->getBettingRound());
			$g->nextRound();
			$this->assertEquals(0, strlen($g->getBoardStr()));
		}

		public function testMultiplePots() {
			$g = new Game();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->addPlayer($p3);

			$g->setInitialStackSize(100);
			$g->setBlinds(5, 10);

			$p2->addMoney(100);
			$p3->addMoney(200);

			$g->stackDeck("Ah Ad Kh Kd Jh Td 2h 3d 6c 7s 9h");

			$g->beginGame();

			$this->assertEquals(100, $p1->getMoney());
			$this->assertEquals(200, $p2->getMoney());
			$this->assertEquals(300, $p3->getMoney());

			$g->postBlinds();

			$this->assertEquals(null, $g->allIn($p1));
			$this->assertEquals(null, $g->allIn($p2));
			$this->assertEquals(null, $g->getWinners());
			$this->assertEquals(null, $g->allIn($p3));

			// -- showdown

			// p1 has the best hand, p2 the second best, p3 loses

			$this->assertEquals(300, $p1->getMoney());
			$this->assertEquals(200, $p2->getMoney());
			$this->assertEquals(100, $p3->getMoney());

			$winners = $g->getWinners();
			$this->assertEquals(3, count($winners));
			$this->assertEquals(300, $winners['Player 1']);
			$this->assertEquals(200, $winners['Player 2']);
			$this->assertEquals(100, $winners['Player 3']);

			$muckers = $g->getMuckers();
			$this->assertEquals(0, count($muckers));

			$this->assertEquals(-2, $g->getBettingRound());
			$g->nextRound();
			$this->assertEquals(0, strlen($g->getBoardStr()));
		}

		public function testMultiplePots2() {
			$g = new Game();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->addPlayer($p3);

			$g->setInitialStackSize(100);
			$g->setBlinds(5, 10);

			$p2->addMoney(100);
			$p3->addMoney(200);

			$g->stackDeck("Ah Ad Kh Kd Jh Td 2h 3d 6c 7s 9h");

			$g->beginGame();

			$this->assertEquals(100, $p1->getMoney());
			$this->assertEquals(200, $p2->getMoney());
			$this->assertEquals(300, $p3->getMoney());

			$g->postBlinds();

			// Go to the flop
			$this->assertEquals(null, $g->call($p1));
			$this->assertEquals(null, $g->call($p2));
			$this->assertEquals(null, $g->check($p3));

			$this->assertEquals(8, strlen($g->getBoardStr()));

			$this->assertEquals(null, $g->allIn($p2));
			$this->assertEquals(null, $g->allIn($p3));
			$this->assertEquals(8, strlen($g->getBoardStr()));
			$this->assertEquals(null, $g->allIn($p1));


			// -- showdown

			// p1 has the best hand, p2 the second best, p3 loses

			$this->assertEquals(300, $p1->getMoney());
			$this->assertEquals(200, $p2->getMoney());
			$this->assertEquals(100, $p3->getMoney());
		}

		public function testAllIn3() {
			$g = new Game();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->addPlayer($p3);

			$g->setInitialStackSize(100);
			$g->setBlinds(5, 10);

			$p1->addMoney(200);
			$p2->addMoney(100);

			$g->stackDeck("Jh Td Kh Kd Ah Ad 2h 3d 6c 7s 9h");

			$g->beginGame();

			$g->postBlinds();

			$this->assertEquals(null, $g->allIn($p1));
			$this->assertEquals(null, $g->allIn($p2));
			$this->assertEquals(null, $g->allIn($p3));

			// -- showdown

			$this->assertEquals(100, $p1->getMoney());
			$this->assertEquals(200, $p2->getMoney());
			$this->assertEquals(300, $p3->getMoney());
		}

		function testHeadsUp() {
			$g = new Game();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$g->addPlayer($p1);
			$g->addPlayer($p2);

			$g->setInitialStackSize(100);
			$g->setBlinds(5, 10);

			$g->beginGame();

			$this->assertTrue($g->getCurrentPlayer()->equals($p1), "Current player is p1");

			$g->postBlinds();

			$this->assertTrue($g->getCurrentPlayer()->equals($p1), "Current player is p1");

			$this->assertTrue($g->getCurrentDealer()->equals($p1), "Dealer is p1");
			$this->assertTrue($g->getCurrentSB()->equals($p1), "SB is p1");
			$this->assertTrue($g->getCurrentBB()->equals($p2), "BB is p2");

			$this->assertEquals(null, $g->call($p1));
			$this->assertEquals(0, strlen($g->getBoardStr()));
			$this->assertEquals("It's not your turn", $g->check($p1));
			$this->assertEquals(null, $g->check($p2));

			// -- flop

			$this->assertEquals(8, strlen($g->getBoardStr()));

			$this->assertTrue($g->getCurrentDealer()->equals($p1), "Dealer is p1");
			$this->assertTrue($g->getCurrentSB()->equals($p1), "SB is p1");
			$this->assertTrue($g->getCurrentBB()->equals($p2), "BB is p2");

			$this->assertEquals(null, $g->check($p2));
			$this->assertEquals(null, $g->check($p1));

			// turn
			$this->assertEquals(null, $g->check($p2));
			$this->assertEquals(null, $g->check($p1));

			// river
			$this->assertEquals(null, $g->check($p2));
			$this->assertEquals(null, $g->check($p1));

			$this->assertEquals(4, $g->getBettingRound());
			$g->nextRound();
			$this->assertEquals(0, strlen($g->getBoardStr()));

			// next round
			$g->postBlinds();
		}

		public function testEliminatePlayer() {
			$g = new Game();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->addPlayer($p3);

			$g->setInitialStackSize(100);
			$g->setBlinds(5, 10);

			$g->stackDeck("Ah Ad Kh Kd Jh Td 2h 3d 6c 7s 9h");

			$g->beginGame();

			$g->postBlinds();

			$this->assertEquals("Player 1", $g->getCurrentDealer()->getName());

			$this->assertEquals(null, $g->allIn($p1));
			$this->assertEquals(null, $g->allIn($p2));
			$this->assertEquals(null, $g->fold($p3));

			$this->assertEquals(210, $p1->getMoney());
			$this->assertEquals(0, $p2->getMoney());
			$this->assertEquals(90, $p3->getMoney());

			$this->assertEquals(-2, $g->getBettingRound());
			$g->nextRound();

			$this->assertEquals("Player 3", $g->getCurrentPlayer()->getName());
			$this->assertEquals("Player 3", $g->getCurrentSB()->getName());
			$this->assertEquals("Player 1", $g->getCurrentBB()->getName());

			$g->postBlinds();

			// Player 3 is actually not the dealer, player 2 is.
			// It's possible to be the dealer while eliminated IRL.
			//$this->assertEquals("Player 3", $g->getCurrentDealer()->getName());

			$this->assertEquals(200, $p1->getMoney());
			$this->assertEquals(0, $p2->getMoney());
			$this->assertEquals(85, $p3->getMoney());

			$this->assertEquals("You are not in the game", $g->call($p2));

			$this->assertEquals("Player 3", $g->getCurrentPlayer()->getName());
			$this->assertEquals(null, $g->call($p3));
			$this->assertEquals(null, $g->check($p1));

			// -- flop
			$this->assertEquals("Jh Td 2h", $g->getBoardStr());
			$this->assertEquals(null, $g->allIn($p3));

			$this->assertEquals(null, $g->getWinners());

			$this->assertEquals(8, strlen($g->getBoardStr()));

			$this->assertEquals(null, $g->call($p1));

			// -- showdown
			$this->assertEquals(300, $p1->getMoney());
			$this->assertEquals(0, $p2->getMoney());
			$this->assertEquals(0, $p3->getMoney());

			$winners = $g->getWinners();
			$this->assertEquals(1, count($winners));
			foreach($winners as $name=>$amt) {
				$this->assertEquals($p1->getName(), $name);
				$this->assertEquals(180, $amt);
			}
		}

		public function testCheckThrough() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$g = new Game();
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->beginGame();

			$g->postBlinds();

			$this->assertEquals("", $g->getBoardStr());
			$g->call($p1);
			$this->assertEquals("", $g->getBoardStr());
			$g->check($p2);
			// flop
			$this->assertEquals(8, strlen($g->getBoardStr()));
			$this->assertEquals("", $g->check($p2)); // p2 goes first here since p1 is dealer, and they're only swapped on the blinding round
			$this->assertEquals(8, strlen($g->getBoardStr()));
			$this->assertEquals("", $g->check($p1));
			// turn
			$this->assertEquals(11, strlen($g->getBoardStr()));
			$this->assertEquals("", $g->check($p2));
			$this->assertEquals(11, strlen($g->getBoardStr()));
			$this->assertEquals("", $g->check($p1));
			// river
			$this->assertEquals(14, strlen($g->getBoardStr()));
			$this->assertEquals("", $g->check($p2));
			$this->assertEquals(14, strlen($g->getBoardStr()));
			$this->assertEquals("", $g->check($p1));
			$this->assertEquals(4, $g->getBettingRound());
			$g->nextRound();
			$this->assertEquals(0, strlen($g->getBoardStr()));

		}

		public function testMuckers() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$g = new Game();
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->stackDeck("Ah Ad Kh Kd 6h 7c 8d 4c Qh");
			$g->beginGame();

			$g->postBlinds();
			$this->assertEquals("", $g->call($p1));;
			$this->assertEquals("", $g->check($p2));
			// flop
			$this->assertEquals("", $g->check($p2));
			$this->assertEquals("", $g->check($p1));
			// turn
			$this->assertEquals("", $g->check($p2));
			$this->assertEquals("", $g->check($p1));
			// river
			$this->assertEquals("", $g->check($p2));
			$this->assertEquals("", $g->check($p1));

			$winners = $g->getWinners();
			$muckers = $g->getMuckers();
			$this->assertEquals(1, count($winners));
			$this->assertEquals(20, $winners['Player 1']);
			$this->assertEquals(0, count($muckers));
		}

		public function testMuckers2() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$g = new Game();
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->stackDeck("Kh Kd Ah Ad 6h 7c 8d 4c Qh");
			$g->beginGame();

			$g->postBlinds();
			$g->call($p1);
			$g->check($p2);
			// flop
			$g->check($p2);
			$g->check($p1);
			// turn
			$g->check($p2);
			$g->check($p1);
			// river
			$g->check($p2);
			$g->check($p1);

			$winners = $g->getWinners();
			$muckers = $g->getMuckers();
			$this->assertEquals(1, count($winners));
			$this->assertEquals(20, $winners['Player 2']);
			$this->assertEquals(1, count($muckers));
			$this->assertEquals("Player 1", $muckers[0]);
		}

		public function testMuckers3() {
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$g = new Game();
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->addPlayer($p3);
			$g->stackDeck("Kh Kd Ah Ad Th Td 6h 7c 8d 4c Qh");
			$g->beginGame();

			$g->postBlinds();
			$g->call($p1);
			$g->call($p2);
			$g->check($p3);
			// flop
			$g->check($p2);
			$g->check($p3);
			$g->check($p1);
			// turn
			$g->check($p2);
			$g->check($p3);
			$g->check($p1);
			// river
			$g->check($p2);
			$g->check($p3);
			$g->check($p1);

			$winners = $g->getWinners();
			$muckers = $g->getMuckers();
			$this->assertEquals(1, count($winners));
			$this->assertEquals(30, $winners['Player 2']);
			$this->assertEquals(2, count($muckers));
			$this->assertEquals("Player 3", $muckers[0]);
			$this->assertEquals("Player 1", $muckers[1]);
		}

		public function testSmallBlindFold() {
			$g = new Game();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$g->addPlayer($p1);
			$g->addPlayer($p2);

			$g->setInitialStackSize(100);
			$g->setBlinds(5, 10);

			$g->beginGame();

			$this->assertTrue($g->getCurrentPlayer()->equals($p1), "Current player is p1");

			$g->postBlinds();

			$this->assertTrue($g->getCurrentPlayer()->equals($p1), "Current player is p1");

			$this->assertTrue($g->getCurrentDealer()->equals($p1), "Dealer is p1");
			$this->assertTrue($g->getCurrentSB()->equals($p1), "SB is p1");
			$this->assertTrue($g->getCurrentBB()->equals($p2), "BB is p2");

			$this->assertEquals(null, $g->fold($p1));
			$this->assertEquals(0, strlen($g->getBoardStr()));
			$winners = $g->getWinners();
			$muckers = $g->getMuckers();
			$this->assertEquals(1, count($winners));
			$this->assertEquals(15, $winners['Player 2']);
			$this->assertEquals(0, count($muckers));
		}

		public function testCrash1() {
			// Player 3 died on an all-in, and got an exception on the next round
			// Exception: postBlinds: res is It's not your turnYou can't bet nothing!
			$g = new Game();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->addPlayer($p3);
			$g->setInitialStackSize(500);
			$g->setBlinds(5, 10);

			$g->stackDeck("Ah Ad Kh Kd Jh Td 2h 3d 6c 7s 9h");

			$g->beginGame();

			$g->postBlinds();
			$this->assertTrue($g->getCurrentPlayer()->equals($p1), "Current player is p1");

			$g->allIn($p1);
			$g->fold($p2);
			$g->allIn($p3);

			// P3 is eliminated...

			$g->nextRound();

			$this->assertEquals("Player 1", $g->getCurrentBB()->getName());
			$this->assertEquals(null, $g->getCurrentSB()); // dead small

			$g->postBlinds();
		}

		public function testMinRaise() {
			$g = new Game();
			$p1 = new Player("Player 1");
			$p2 = new Player("Player 2");
			$p3 = new Player("Player 3");
			$p4 = new Player("Player 4");
			$g->addPlayer($p1);
			$g->addPlayer($p2);
			$g->addPlayer($p3);
			$g->addPlayer($p4);
			$g->setInitialStackSize(500);
			$g->setBlinds(5, 10);

			$g->stackDeck("Ah Ad Kh Kd Jh Td 2h 3d 6c 7s 9h");

			$g->beginGame();

			$g->postBlinds(); // p2 is small, p3 is big
			$this->assertTrue($g->getCurrentPlayer()->equals($p4), "Current player is p4");
			$this->assertEquals(null, $g->call($p4));
			$this->assertEquals(null, $g->call($p1));
			$this->assertEquals("The minimum raise is $10", $g->raise($p2, 5));
			$this->assertEquals(null, $g->raise($p2, 10));
			$this->assertEquals("The minimum raise is $10", $g->raise($p3, 5));
			$this->assertEquals(null, $g->raise($p3, 20));
			$this->assertEquals("The minimum raise is $20", $g->raise($p4, 5));
			$this->assertEquals(null, $g->raise($p4, 30));
			$this->assertEquals(null, $g->call($p1));
			$this->assertEquals(null, $g->call($p2));
			$this->assertEquals(null, $g->call($p3));

			// flop
			$this->assertTrue($g->getCurrentPlayer()->equals($p2), "Current player is p2");
			$this->assertEquals("The minimum raise is $10", $g->raise($p2, 5));
			$this->assertEquals(null, $g->raise($p2, 10));
			$this->assertEquals("The minimum raise is $10", $g->raise($p3, 5));
			$this->assertEquals(null, $g->raise($p3, 20));
			$this->assertEquals("The minimum raise is $20", $g->raise($p4, 5));
			$this->assertEquals(null, $g->raise($p4, 30));
			$this->assertEquals(null, $g->call($p1));
			$this->assertEquals(null, $g->call($p2));
			$this->assertEquals(null, $g->call($p3));

			// turn
			$this->assertTrue($g->getCurrentPlayer()->equals($p2), "Current player is p2");
			$this->assertEquals("The minimum raise is $10", $g->raise($p2, 5));
			$this->assertEquals(null, $g->raise($p2, 10));
			$this->assertEquals("The minimum raise is $10", $g->raise($p3, 5));
			$this->assertEquals(null, $g->raise($p3, 20));
			$this->assertEquals("The minimum raise is $20", $g->raise($p4, 5));
			$this->assertEquals(null, $g->raise($p4, 30));
			$this->assertEquals(null, $g->call($p1));
			$this->assertEquals(null, $g->call($p2));
			$this->assertEquals(null, $g->call($p3));

			// river
			$this->assertTrue($g->getCurrentPlayer()->equals($p2), "Current player is p2");
			$this->assertEquals("The minimum raise is $10", $g->raise($p2, 5));
			$this->assertEquals(null, $g->raise($p2, 10));
			$this->assertEquals("The minimum raise is $10", $g->raise($p3, 5));
			$this->assertEquals(null, $g->raise($p3, 20));
			$this->assertEquals("The minimum raise is $20", $g->raise($p4, 5));
			$this->assertEquals(null, $g->raise($p4, 30));
			$this->assertEquals(null, $g->call($p1));
			$this->assertEquals(null, $g->call($p2));
			$this->assertEquals(null, $g->call($p3));
		}
	}
?>
