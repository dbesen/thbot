<?
	require_once 'PHPUnit/Framework.php';

	require_once("../code/autoloader.php");

	class IrcHandlerTest extends PHPUnit_Framework_TestCase {
		private static $pub = array();
		private static $private = array();

		public function testHelp() {
			$irch = new IrcHandler(array($this, "onPublic"), array($this, "onPrivate"));
			$irch->publicMessage("player1", "!help");
			$this->verifyPublic("!holdem to start a game");
		}

		public function testTwoBegins() {
			$irch = new IrcHandler(array($this, "onPublic"), array($this, "onPrivate"));
			$irch->publicMessage("player1", "!holdem");
			$this->verifyPublic("Starting game in 30 seconds");
			$this->verifyPublic("Message me \"buy in\" to register");
			$this->verifyPublic("player1 has registered");
			$irch->publicMessage("player1", "!holdem");
			$this->verifyPublic("Game already in progress");
		}

		public function testNotEnoughPlayers() {
			$irch = new IrcHandler(array($this, "onPublic"), array($this, "onPrivate"));
			$irch->publicMessage("player1", "!holdem");
			$this->verifyPublic("Starting game in 30 seconds");
			$this->verifyPublic("Message me \"buy in\" to register");
			$this->verifyPublic("player1 has registered");
			$irch->beginGame();
			$this->verifyPublic("Not enough players, aborting game");

			// Do it twice, in case the abort failed somehow
			$irch->publicMessage("player1", "!holdem");
			$this->verifyPublic("Starting game in 30 seconds");
			$this->verifyPublic("Message me \"buy in\" to register");
			$this->verifyPublic("player1 has registered");
			$irch->beginGame();
			$this->verifyPublic("Not enough players, aborting game");
		}

		public function testBuyIn() {
			$irch = new IrcHandler(array($this, "onPublic"), array($this, "onPrivate"));
			$irch->publicMessage("player1", "!holdem");
			$this->verifyPublic("Starting game in 30 seconds");
			$this->verifyPublic("Message me \"buy in\" to register");
			$this->verifyPublic("player1 has registered");
			$irch->privateMessage("player1", "buyin");
			$this->verifyPrivate("player1", "You're already registered");
			$irch->privateMessage("player2", "buyin");
			$this->verifyPublic("player2 has registered");
			$irch->privateMessage("player3", "Buyin");
			$this->verifyPublic("player3 has registered");
			$irch->privateMessage("player4", "BUY IN");
			$this->verifyPublic("player4 has registered");
			$irch->privateMessage("player5", "BUYIN");
			$this->verifyPublic("player5 has registered");
			$irch->privateMessage("player5", "buyin");
			$this->verifyPrivate("player5", "You're already registered");
		}

		public function testRunGame() {
			$irch = new IrcHandler(array($this, "onPublic"), array($this, "onPrivate"));
			$irch->publicMessage("player1", "!holdem");
			$this->verifyPublic("Starting game in 30 seconds");
			$this->verifyPublic("Message me \"buy in\" to register");
			$this->verifyPublic("player1 has registered");
			$irch->privateMessage("player2", "buy in");
			$this->verifyPublic("player2 has registered");

			// yeah...
			$irch->stackDeck("Ah Ad As Ac Kh 4c Kd Jh 5c");

			// The IRC interface is what does the waiting, so
			// it has to have a callback to call to end the registration round.
			// This means it has to have an (untested) check for the !holdem
			// call, and it starts its timer then.
			$irch->beginGame();
			$this->verifyPublic("Registration over, beginning game");
			$this->verifyPublic("To play, message me commands: bet <amt>, raise <amt>, call, fold, check, all in");
			$this->verifyPublic("Players: player1 player2");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 1");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("D/SB  player1         $500");
			$this->verifyPublic("BB    player2         $500");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,03Ac");
			$this->verifyPublic("player1 posts $5 for the small blind");
			$this->verifyPublic("player2 posts $10 for the big blind");
			$this->verifyAction("player1");
			$irch->privateMessage("player1", "call");
			$this->verifyPublic("player1 calls");
			$this->verifyPublic("Main pot: $20");
			$this->verifyAction("player2");
			$irch->privateMessage("player2", "check");
			$this->verifyPublic("player2 checks");
			$this->verifyPublic(" ");
			$this->verifyPublic("The flop comes: 01,05Kh 01,034c 00,02Kd");
			$this->verifyPublic(" ");
			$this->verifyPublic("Main pot: $20");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("D/SB  player1         $490");
			$this->verifyPublic("BB    player2         $490");
			$this->verifyAction("player2");
			$irch->privateMessage("player1", "bet 100");
			$this->verifyPrivate("player1", "It's not your turn");
			$irch->privateMessage("player2", "bet 100");
			$this->verifyPublic("player2 bets $100");
			$this->verifyPublic("Main pot: $120");
			$this->verifyAction("player1");
			$irch->privateMessage("player1", "raise $100");
			$this->verifyPublic("player1 raises $100");
			$this->verifyPublic("Main pot: $320");
			$this->verifyAction("player2");
			$irch->privateMessage("player2", "call");
			$this->verifyPublic("player2 calls");
			$this->verifyPublic(" ");
			$this->verifyPublic("The turn comes: 01,05Kh 01,034c 00,02Kd 01,05Jh");
			$this->verifyPublic(" ");
			$this->verifyPublic("Main pot: $420");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("D/SB  player1         $290");
			$this->verifyPublic("BB    player2         $290");
			$this->verifyAction("player2");
			$irch->privateMessage("player2", "check");
			$this->verifyPublic("player2 checks");
			$this->verifyPublic("Main pot: $420");
			$this->verifyAction("player1");
			$irch->privateMessage("player1", "check");
			$this->verifyPublic("player1 checks");
			$this->verifyPublic(" ");
			$this->verifyPublic("The river comes: 01,05Kh 01,034c 00,02Kd 01,05Jh 01,035c");
			$this->verifyPublic(" ");
			$this->verifyPublic("Main pot: $420");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("D/SB  player1         $290");
			$this->verifyPublic("BB    player2         $290");
			$this->verifyAction("player2");
			$irch->privateMessage("player2", "all in");
			$this->verifyPublic("player2 goes all in");
			$this->verifyPublic("Main pot: $710");
			$this->verifyAction("player1");
			$irch->privateMessage("player1", "call");
			$this->verifyPublic("player1 calls");
			$this->verifyPublic(" ");
			$this->verifyPublic("All-In Showdown");
			$this->verifyPublic("---------------");
			$this->verifyPublic("player1 wins $500 with: 01,05Ah 00,02Ad (Two pair: Aces and kings)");
			$this->verifyPublic("player2 wins $500 with: 01,15As 01,03Ac (Two pair: Aces and kings)");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 2");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("BB    player1         $500");
			$this->verifyPublic("D/SB  player2         $500");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPublic("player2 posts $5 for the small blind");
			$this->verifyPublic("player1 posts $10 for the big blind");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,03Ac");
			$this->verifyAction("player2");
		}

		public function testMucking() {
			$this->testLast(); // we're gonna reset messages...
			$irch = new IrcHandler(array($this, "onPublic"), array($this, "onPrivate"));
			$irch->publicMessage("player1", "!holdem");
			$irch->privateMessage("player2", "buy in");

			$irch->stackDeck("Ah 4d As Ac Kh 4c Kd Jh 5c");

			$irch->beginGame();
			$irch->privateMessage("player1", "call");
			$irch->privateMessage("player2", "check");
			// flop
			$irch->privateMessage("player2", "bet 100");
			$irch->privateMessage("player1", "raise $100");
			$irch->privateMessage("player2", "call");
			// turn
			$irch->privateMessage("player2", "check");
			$irch->privateMessage("player1", "check");
			// river
			$irch->privateMessage("player2", "check");
			IrcHandlerTest::$pub = array(); // Clear all the messages we skipped
			IrcHandlerTest::$private = array();
			$irch->privateMessage("player1", "check");
			// showdown
			$this->verifyPublic("player1 checks");
			$this->verifyPublic(" ");
			$this->verifyPublic("Showdown");
			$this->verifyPublic("--------");
			$this->verifyPublic("player2 wins $420 with: 01,15As 01,03Ac (Two pair: Aces and kings)");
			$this->verifyPublic("player1 mucks");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 2");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("BB    player1         $290");
			$this->verifyPublic("D/SB  player2         $710");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPublic("player2 posts $5 for the small blind");
			$this->verifyPublic("player1 posts $10 for the big blind");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,024d");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,03Ac");
			$this->verifyAction("player2");
		}

		public function testBadInput() {
			$irch = new IrcHandler(array($this, "onPublic"), array($this, "onPrivate"));
			$irch->publicMessage("player1", "!holdem");
			$this->verifyPublic("Starting game in 30 seconds");
			$this->verifyPublic("Message me \"buy in\" to register");
			$this->verifyPublic("player1 has registered");
			$irch->privateMessage("player2", "buy in");
			$this->verifyPublic("player2 has registered");

			$irch->stackDeck("Ah Ad As Ac Kh 4c Kd Jh 5c");

			$irch->beginGame();
			$this->verifyPublic("Registration over, beginning game");
			$this->verifyPublic("To play, message me commands: bet <amt>, raise <amt>, call, fold, check, all in");
			$this->verifyPublic("Players: player1 player2");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 1");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("D/SB  player1         $500");
			$this->verifyPublic("BB    player2         $500");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,03Ac");
			$this->verifyPublic("player1 posts $5 for the small blind");
			$this->verifyPublic("player2 posts $10 for the big blind");
			$this->verifyAction("player1");
			$irch->privateMessage("asdf", "call");
			$this->verifyPrivate("asdf", "You're not in the game");
			$irch->privateMessage("asdf", "bet 20");
			$this->verifyPrivate("asdf", "You're not in the game");
			$irch->privateMessage("asdf", "raise 20");
			$this->verifyPrivate("asdf", "You're not in the game");
			$irch->privateMessage("asdf", "all in");
			$this->verifyPrivate("asdf", "You're not in the game");
			$irch->privateMessage("asdf", "fold");
			$this->verifyPrivate("asdf", "You're not in the game");
			$irch->privateMessage("asdf", "check");
			$this->verifyPrivate("asdf", "You're not in the game");
			$irch->privateMessage("player1", "bet 900");
			$this->verifyPrivate("player1", "You don't have enough money for that");
			$irch->privateMessage("player1", "bet");
			$this->verifyPrivate("player1", "Bet needs an amount");
			$irch->privateMessage("player1", "raise");
			$this->verifyPrivate("player1", "Raise needs an amount");
			$irch->privateMessage("player1", "raise 0");
			$this->verifyPrivate("player1", "You can't raise nothing!");
			$irch->privateMessage("player1", "bet 0");
			$this->verifyPrivate("player1", "You can't bet nothing!");
			$irch->privateMessage("player1", "bet asdf");
			$this->verifyPrivate("player1", "You can't bet nothing!");
			$irch->privateMessage("player1", "raise -30");
			$this->verifyPrivate("player1", "You can't raise a negative amount!");
			$irch->privateMessage("player1", "bet -30");
			$this->verifyPrivate("player1", "You can't bet a negative amount!");
			$irch->privateMessage("player1", "call");
			$this->verifyPublic("player1 calls");
			$this->verifyPublic("Main pot: $20");
			$this->verifyAction("player2");
			$irch->privateMessage("player2", "call");
			$this->verifyPrivate("player2", "You can't call now -- did you mean check?");
			$irch->privateMessage("player2", "asdf");
			$this->verifyPrivate("player2", "Unrecognized command");
		}

		public function testSmallBlindFold() {
			$irch = new IrcHandler(array($this, "onPublic"), array($this, "onPrivate"));
			$irch->publicMessage("player1", "!holdem");
			$this->verifyPublic("Starting game in 30 seconds");
			$this->verifyPublic("Message me \"buy in\" to register");
			$this->verifyPublic("player1 has registered");
			$irch->privateMessage("player2", "buy in");
			$this->verifyPublic("player2 has registered");
			$irch->stackDeck("Ah Ad As Ac Kh 4c Kd Jh 5c");
			$irch->beginGame();

			$this->verifyPublic("Registration over, beginning game");
			$this->verifyPublic("To play, message me commands: bet <amt>, raise <amt>, call, fold, check, all in");
			$this->verifyPublic("Players: player1 player2");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 1");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("D/SB  player1         $500");
			$this->verifyPublic("BB    player2         $500");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,03Ac");
			$this->verifyPublic("player1 posts $5 for the small blind");
			$this->verifyPublic("player2 posts $10 for the big blind");
			$this->verifyAction("player1");
			$irch->privateMessage("player1", "fold");
			$this->verifyPublic("player1 folds");
			$this->verifyPublic(" ");
			$this->verifyPublic("player2 wins $15 uncontested");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 2");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("BB    player1         $495");
			$this->verifyPublic("D/SB  player2         $505");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPublic("player2 posts $5 for the small blind");
			$this->verifyPublic("player1 posts $10 for the big blind");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,03Ac");
			$this->verifyAction("player2");
		}

		public function testOverallWinner() {
			$this->testLast(); // we're gonna reset messages...
			$irch = new IrcHandler(array($this, "onPublic"), array($this, "onPrivate"));
			$irch->publicMessage("player1", "!holdem");
			$irch->privateMessage("player2", "buy in");

			$irch->stackDeck("Ah 4d As Ac Kh 4c Kd Jh 5c");

			$irch->beginGame();
			$irch->privateMessage("player1", "call");
			$irch->privateMessage("player2", "check");
			// flop
			$irch->privateMessage("player2", "bet 100");
			$irch->privateMessage("player1", "raise $100");
			$irch->privateMessage("player2", "call");
			// turn
			$irch->privateMessage("player2", "check");
			$irch->privateMessage("player1", "check");
			// river
			$irch->privateMessage("player2", "all in");
			IrcHandlerTest::$pub = array(); // Clear all the messages we skipped
			IrcHandlerTest::$private = array();
			$irch->privateMessage("player1", "call");
			// showdown
			$this->verifyPublic("player1 calls");
			$this->verifyPublic(" ");
			$this->verifyPublic("All-In Showdown");
			$this->verifyPublic("---------------");
			$this->verifyPublic("player2 wins $1000 with: 01,15As 01,03Ac (Two pair: Aces and kings)");
			$this->verifyPublic("player1 had: 01,05Ah 00,024d (Two pair: Kings and fours)");
			$this->verifyPublic(" ");
			$this->verifyPublic("Game over");
			$this->verifyPublic("Winner: player2");

			// Next game
			$irch->publicMessage("player1", "!holdem");
			$this->verifyPublic("Starting game in 30 seconds");
			$this->verifyPublic("Message me \"buy in\" to register");
			$this->verifyPublic("player1 has registered");
			$irch->privateMessage("player2", "buy in");
			$this->verifyPublic("player2 has registered");
			$irch->stackDeck("Ah Ad As Ac Kh 4c Kd Jh 5c");
			$irch->beginGame();

			$this->verifyPublic("Registration over, beginning game");
			$this->verifyPublic("To play, message me commands: bet <amt>, raise <amt>, call, fold, check, all in");
			$this->verifyPublic("Players: player1 player2");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 1");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("D/SB  player1         $500");
			$this->verifyPublic("BB    player2         $500");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,03Ac");
			$this->verifyPublic("player1 posts $5 for the small blind");
			$this->verifyPublic("player2 posts $10 for the big blind");
			$this->verifyAction("player1");
		}

		public function testThreePlayers() {
			$irch = new IrcHandler(array($this, "onPublic"), array($this, "onPrivate"));
			$irch->publicMessage("player1", "!holdem");
			$this->verifyPublic("Starting game in 30 seconds");
			$this->verifyPublic("Message me \"buy in\" to register");
			$this->verifyPublic("player1 has registered");
			$irch->privateMessage("player2", "buy in");
			$this->verifyPublic("player2 has registered");
			$irch->privateMessage("player3", "buy in");
			$this->verifyPublic("player3 has registered");

			// yeah...
			$irch->stackDeck("Ah Ad As 4c 2h 3c Kh 4c Kd Jh 5c");

			// The IRC interface is what does the waiting, so
			// it has to have a callback to call to end the registration round.
			// This means it has to have an (untested) check for the !holdem
			// call, and it starts its timer then.
			$irch->beginGame();
			$this->verifyPublic("Registration over, beginning game");
			$this->verifyPublic("To play, message me commands: bet <amt>, raise <amt>, call, fold, check, all in");
			$this->verifyPublic("Players: player1 player2 player3");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 1");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("D     player1         $500");
			$this->verifyPublic("SB    player2         $500");
			$this->verifyPublic("BB    player3         $500");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,034c");
			$this->verifyPrivate("player3", "You were dealt: 01,052h 01,033c");
			$this->verifyPublic("player2 posts $5 for the small blind");
			$this->verifyPublic("player3 posts $10 for the big blind");
			$this->verifyAction("player1");
			$irch->privateMessage("player1", "call");
			$this->verifyPublic("player1 calls");
			$this->verifyAction("player2");
			$this->verifyPublic("Main pot: $25");
			$irch->privateMessage("player2", "fold");
			$this->verifyPublic("player2 folds");
			$this->verifyAction("player3");
			$this->verifyPublic("Main pot: $25");
			$irch->privateMessage("player3", "fold");
			$this->verifyPublic("player3 folds");
			$this->verifyPublic(" ");
			$this->verifyPublic("player1 wins $25 uncontested");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 2");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("BB    player1         $515");
			$this->verifyPublic("D     player2         $495");
			$this->verifyPublic("SB    player3         $490");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,034c");
			$this->verifyPrivate("player3", "You were dealt: 01,052h 01,033c");
			$this->verifyPublic("player3 posts $5 for the small blind");
			$this->verifyPublic("player1 posts $10 for the big blind");
			$this->verifyAction("player2");

			// We want to test that player3 can go all in by calling with not enough money
			$irch->privateMessage("player2", "fold");
			$this->verifyPublic("player2 folds");
			$this->verifyAction("player3");
			$this->verifyPublic("Main pot: $15");
			$irch->privateMessage("player3", "call");
			$this->verifyPublic("player3 calls");
			$this->verifyAction("player1");
			$this->verifyPublic("Main pot: $20");
			$irch->privateMessage("player1", "all in");
			$this->verifyPublic("player1 goes all in");
			$this->verifyAction("player3");
			$this->verifyPublic("Main pot: $525");
			$irch->privateMessage("player3", "call");
			$this->verifyPublic("player3 calls");
			$this->verifyPublic(" ");
			$this->verifyPublic("All-In Showdown");
			$this->verifyPublic("---------------");
			$this->verifyPublic("player1 has: 01,05Ah 00,02Ad");
			$this->verifyPublic("player3 has: 01,052h 01,033c");
			$this->verifyPublic(" ");
			$this->verifyPublic("The flop comes: 01,05Kh 01,034c 00,02Kd");
			$this->verifyPublic("The turn comes: 01,05Kh 01,034c 00,02Kd 01,05Jh");
			$this->verifyPublic("The river comes: 01,05Kh 01,034c 00,02Kd 01,05Jh 01,035c");
			$this->verifyPublic(" ");
			$this->verifyPublic("player1 wins $1005 with: 01,05Ah 00,02Ad (Two pair: Aces and kings)");
			$this->verifyPublic("player3 had: 01,052h 01,033c (Pair: Kings)");
			$this->verifyPublic(" ");

			// Note that the positions of the small and big blind aren't swapped here
			// even though there's two players -- because there's a dead dealer
			$this->verifyPublic("Round 3");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("SB    player1        $1005");
			$this->verifyPublic("BB    player2         $495");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,034c");
			$this->verifyPublic("player1 posts $5 for the small blind");
			$this->verifyPublic("player2 posts $10 for the big blind");
			$this->verifyAction("player1");
		}

		public function testNoShow() {
			$irch = new IrcHandler(array($this, "onPublic"), array($this, "onPrivate"));
			$irch->publicMessage("player1", "!holdem");
			$this->verifyPublic("Starting game in 30 seconds");
			$this->verifyPublic("Message me \"buy in\" to register");
			$this->verifyPublic("player1 has registered");
			$irch->privateMessage("player2", "buy in");
			$this->verifyPublic("player2 has registered");
			$irch->privateMessage("player3", "buy in");
			$this->verifyPublic("player3 has registered");

			$irch->stackDeck("Ah Ad As Ac 2h 3c Kh 4c Kd Jh 5c");

			$irch->beginGame();
			$this->verifyPublic("Registration over, beginning game");
			$this->verifyPublic("To play, message me commands: bet <amt>, raise <amt>, call, fold, check, all in");
			$this->verifyPublic("Players: player1 player2 player3");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 1");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("D     player1         $500");
			$this->verifyPublic("SB    player2         $500");
			$this->verifyPublic("BB    player3         $500");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,03Ac");
			$this->verifyPrivate("player3", "You were dealt: 01,052h 01,033c");
			$this->verifyPublic("player2 posts $5 for the small blind");
			$this->verifyPublic("player3 posts $10 for the big blind");
			$this->verifyAction("player1");
			$irch->privateMessage("player1", "call");
			$this->verifyPublic("player1 calls");
			$this->verifyAction("player2");
			$this->verifyPublic("Main pot: $25");
			$irch->privateMessage("player2", "call");
			$this->verifyPublic("player2 calls");
			$this->verifyAction("player3");
			$this->verifyPublic("Main pot: $30");
			$irch->privateMessage("player3", "raise 200");
			$this->verifyPublic("player3 raises $200");
			$this->verifyPublic("Main pot: $230");
			$this->verifyAction("player1");
			$irch->privateMessage("player1", "all in");
			$this->verifyPublic("player1 goes all in");
			$this->verifyPublic("Main pot: $720");
			$this->verifyAction("player2");
			$irch->privateMessage("player2", "all in");
			$this->verifyPublic("player2 goes all in");
			$this->verifyPublic("Main pot: $1210");
			$this->verifyAction("player3");
			$irch->privateMessage("player3", "fold");
			$this->verifyPublic("player3 folds");
			$this->verifyPublic(" ");
			$this->verifyPublic("All-In Showdown");
			$this->verifyPublic("---------------");
			$this->verifyPublic("player1 has: 01,05Ah 00,02Ad");
			$this->verifyPublic("player2 has: 01,15As 01,03Ac");
			$this->verifyPublic(" ");
			$this->verifyPublic("The flop comes: 01,05Kh 01,034c 00,02Kd");
			$this->verifyPublic("The turn comes: 01,05Kh 01,034c 00,02Kd 01,05Jh");
			$this->verifyPublic("The river comes: 01,05Kh 01,034c 00,02Kd 01,05Jh 01,035c");
			$this->verifyPublic(" ");
			$this->verifyPublic("player1 wins $605 with: 01,05Ah 00,02Ad (Two pair: Aces and kings)");
			$this->verifyPublic("player2 wins $605 with: 01,15As 01,03Ac (Two pair: Aces and kings)");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 2");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("BB    player1         $605");
			$this->verifyPublic("D     player2         $605");
			$this->verifyPublic("SB    player3         $290");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,03Ac");
			$this->verifyPrivate("player3", "You were dealt: 01,052h 01,033c");
			$this->verifyPublic("player3 posts $5 for the small blind");
			$this->verifyPublic("player1 posts $10 for the big blind");
			$this->verifyAction("player2");
			// Player 3 has less money than 1&2
			// 3 goes all in, 1&2 check through to the river
			$irch->privateMessage("player2", "call");
			$this->verifyPublic("player2 calls");
			$this->verifyAction("player3");
			$this->verifyPublic("Main pot: $25");
			$irch->privateMessage("player3", "all in");
			$this->verifyPublic("player3 goes all in");
			$this->verifyAction("player1");
			$this->verifyPublic("Main pot: $310");
			$irch->privateMessage("player1", "call");
			$this->verifyPublic("player1 calls");
			$this->verifyAction("player2");
			$this->verifyPublic("Main pot: $590");
			$irch->privateMessage("player2", "call");
			$this->verifyPublic("player2 calls");
			// Now, we can't show anyone's cards even though 3 is all in since 1 & 2 can still act
			$this->verifyPublic(" ");
			$this->verifyPublic("The flop comes: 01,05Kh 01,034c 00,02Kd");
			$this->verifyPublic(" ");
			$this->verifyPublic("Main pot: $870");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("BB    player1         $315");
			$this->verifyPublic("D     player2         $315");
			$this->verifyPublic("SB    player3           $0");
			$this->verifyAction("player1");
			$irch->privateMessage("player1", "check");
			$this->verifyPublic("player1 checks");
			$this->verifyPublic("Main pot: $870");
			$this->verifyAction("player2");
			$irch->privateMessage("player2", "check");
			$this->verifyPublic("player2 checks");

			$this->verifyPublic(" ");
			$this->verifyPublic("The turn comes: 01,05Kh 01,034c 00,02Kd 01,05Jh");
			$this->verifyPublic(" ");
			$this->verifyPublic("Main pot: $870");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("BB    player1         $315");
			$this->verifyPublic("D     player2         $315");
			$this->verifyPublic("SB    player3           $0");
			$this->verifyAction("player1");
			$irch->privateMessage("player1", "check");
			$this->verifyPublic("player1 checks");
			$this->verifyAction("player2");
			$this->verifyPublic("Main pot: $870");
			$irch->privateMessage("player2", "check");
			$this->verifyPublic("player2 checks");
			$this->verifyPublic(" ");
			$this->verifyPublic("The river comes: 01,05Kh 01,034c 00,02Kd 01,05Jh 01,035c");
			$this->verifyPublic(" ");
			$this->verifyPublic("Main pot: $870");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("BB    player1         $315");
			$this->verifyPublic("D     player2         $315");
			$this->verifyPublic("SB    player3           $0");
			$this->verifyAction("player1");
			$irch->privateMessage("player1", "check");
			$this->verifyPublic("player1 checks");
			$this->verifyAction("player2");
			$this->verifyPublic("Main pot: $870");
			$irch->privateMessage("player2", "check");
			$this->verifyPublic("player2 checks");
			$this->verifyPublic(" ");
			$this->verifyPublic("Showdown");
			$this->verifyPublic("--------");
			$this->verifyPublic("player1 wins $435 with: 01,05Ah 00,02Ad (Two pair: Aces and kings)");
			$this->verifyPublic("player2 wins $435 with: 01,15As 01,03Ac (Two pair: Aces and kings)");
			$this->verifyPublic("player3 had: 01,052h 01,033c (Pair: Kings)");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 3");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("SB    player1         $750");
			$this->verifyPublic("BB    player2         $750");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,03Ac");
			$this->verifyPublic("player1 posts $5 for the small blind");
			$this->verifyPublic("player2 posts $10 for the big blind");
			$this->verifyAction("player1");
		}

		public function testShowdown() {
			// 2 players go all in on the flop
			$irch = new IrcHandler(array($this, "onPublic"), array($this, "onPrivate"));
			$irch->publicMessage("player1", "!holdem");
			$this->verifyPublic("Starting game in 30 seconds");
			$this->verifyPublic("Message me \"buy in\" to register");
			$this->verifyPublic("player1 has registered");
			$irch->privateMessage("player2", "buy in");
			$this->verifyPublic("player2 has registered");
			$irch->stackDeck("Ah Ad As Ac Kh 4c Kd Jh 5c");
			$irch->beginGame();
			$this->verifyPublic("Registration over, beginning game");
			$this->verifyPublic("To play, message me commands: bet <amt>, raise <amt>, call, fold, check, all in");
			$this->verifyPublic("Players: player1 player2");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 1");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("D/SB  player1         $500");
			$this->verifyPublic("BB    player2         $500");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,03Ac");
			$this->verifyPublic("player1 posts $5 for the small blind");
			$this->verifyPublic("player2 posts $10 for the big blind");
			$this->verifyAction("player1");
			$irch->privateMessage("player1", "all in");
			$this->verifyPublic("player1 goes all in");
			$this->verifyAction("player2");
			$this->verifyPublic("Main pot: $510");
			$irch->privateMessage("player2", "all in");
			$this->verifyPublic("player2 goes all in");
			$this->verifyPublic(" ");
			$this->verifyPublic("All-In Showdown");
			$this->verifyPublic("---------------");
			$this->verifyPublic("player1 has: 01,05Ah 00,02Ad");
			$this->verifyPublic("player2 has: 01,15As 01,03Ac");
			$this->verifyPublic(" ");
			$this->verifyPublic("The flop comes: 01,05Kh 01,034c 00,02Kd");
			$this->verifyPublic("The turn comes: 01,05Kh 01,034c 00,02Kd 01,05Jh");
			$this->verifyPublic("The river comes: 01,05Kh 01,034c 00,02Kd 01,05Jh 01,035c");
			$this->verifyPublic(" ");
			$this->verifyPublic("player1 wins $500 with: 01,05Ah 00,02Ad (Two pair: Aces and kings)");
			$this->verifyPublic("player2 wins $500 with: 01,15As 01,03Ac (Two pair: Aces and kings)");
			$this->verifyPublic(" ");
			$this->verifyPublic("Round 2");
			$this->verifyPublic("--------------------------");
			$this->verifyPublic("Pos   Player         Stack");
			$this->verifyPublic("BB    player1         $500");
			$this->verifyPublic("D/SB  player2         $500");
			$this->verifyPublic(" ");
			$this->verifyPublic("Dealing cards...");
			$this->verifyPublic("player2 posts $5 for the small blind");
			$this->verifyPublic("player1 posts $10 for the big blind");
			$this->verifyPrivate("player1", "You were dealt: 01,05Ah 00,02Ad");
			$this->verifyPrivate("player2", "You were dealt: 01,15As 01,03Ac");
			$this->verifyAction("player2");
		}

		public function onPublic($text) {
			array_unshift(IrcHandlerTest::$pub, $text);
		}

		public function onPrivate($playername, $text) {
			array_unshift(IrcHandlerTest::$private, array($playername, $text));
		}

		private function verifyPublic($str) {
			$this->assertTrue(count(IrcHandlerTest::$pub) > 0, "pub has messages");
			$this->assertEquals($str, array_pop(IrcHandlerTest::$pub));
		}

		private function verifyPrivate($nick, $str) {
			$this->assertTrue(count(IrcHandlerTest::$private) > 0, "private has messages");
			$d = array_pop(IrcHandlerTest::$private);
			$this->assertEquals($nick . ": " . $str, $d[0] . ": " . $d[1]);
			$this->assertEquals($nick, $d[0]);
			$this->assertEquals($str, $d[1]);
		}

		private function verifyAction($nick) { // Verify that the given player was notified he has action
			$this->verifyPrivate($nick, "Action is to you.  What would you like to do?");
		}

		public function testLast() { // keep this last
			$this->assertEquals(0, count(IrcHandlerTest::$pub), serialize(IrcHandlerTest::$pub));
			$this->assertEquals(0, count(IrcHandlerTest::$private), serialize(IrcHandlerTest::$private));
		}
	}
?>
